﻿/// <reference path="./TemplateView.d.ts" />
/// <reference path="./TemplateController.d.ts" />
/// <reference path="./DataHelper.ts" />

namespace Mq {

    export class TemplateViewBase implements TemplateView {
        mqController: Mq.ITemplateController = null;

        onPreload(data: Htv.ContentTemplateData,
            onReady: OnTemplateReadyCallback, onError: OnErrorCallback): void {

            if (onReady)
                onReady();
        }

        onLoad(controller: Mq.ITemplateController,
            onReady?: () => void): void {

            if (controller)
                this.mqController = controller;
            if (onReady)
                onReady();
        }

        onShift(counter: number, onEnd: (msg?: (string | Event)) => void): void { }
        onPlay(): void { }
        onPause(): void { }
        onMetadata(metadata: Mq.PlayerMetadata): void { }
        onVolume(volume: number): void { }
        onQueueNext(info: QueueInfo): void { }
        onEditorMessage(message: any): void { }
        onAttribute(attribute: Mq.Attribute): void { }
        onGeoPosition(geoCoords: Mq.GeoPosition): void { }

        // Helpers:
        public static isOtherField(f: Htv.FieldValue): f is Htv.OtherField {
            return Htv.DataHelper.isOtherField(f);
        }
        public static isMediaField(f: Htv.FieldValue): f is Htv.FilesField {
            return Htv.DataHelper.isMediaField(f);
        }

        getDuration(data: Htv.ContentTemplateData): number {
            return Htv.DataHelper.get(data).getDuration(10);
        }

        getFieldValue(data: Htv.ContentTemplateData, fieldName: string, defaultVal?: any, index: number = 0): any {
            return Htv.DataHelper.get(data).getFieldValue(fieldName, defaultVal, index);
        }
        getMediaFieldFiles(data: Htv.ContentTemplateData, fieldName: string, index: number = 0): Htv.ContentFile[] {
            return Htv.DataHelper.get(data).getMediaFieldFiles(fieldName, index);
        }
        getExpiry(data: Htv.ContentTemplateData, defaultExpiryMinutes: number = 0): Date {
            return Htv.DataHelper.get(data).getExpiry(defaultExpiryMinutes);
        }
        getSettingValue(data: Htv.ContentTemplateData, settingName: string, defaultVal?: any): any {
            return Htv.DataHelper.get(data).getSettingValue(settingName, defaultVal);
        }

        isHtml(text: string): boolean {
            let doc = new DOMParser().parseFromString(text, "text/html");
            return [].slice.call(doc.body.childNodes).some(node => node.nodeType === 1);
        }

        addBRTagsIfNotHtml(text: string): string {
            if (!text || this.isHtml(text))
                return text;

            var lines = text.split(/\r\n|\r|\n/);
            for (var i = 0; i < lines.length; i++) {
                lines[i] = (<HTMLElement>document.createElement('a').appendChild(
                    document.createTextNode(lines[i])).parentNode).innerHTML;
            }
            return lines.join('<br/>');
        }

        addPTagsIfNotHtml(text: string): string {
            if (!text || this.isHtml(text))
                return text;

            var lines = text.split(/\r\n|\r|\n/);
            for (var i = 0; i < lines.length; i++) {
                lines[i] = '<p>' + (<HTMLElement>document.createElement('a').appendChild(
                    document.createTextNode(lines[i])).parentNode).innerHTML + '</p>';
            }
            return lines.join('');
        }
    }
}