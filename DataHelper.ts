/// <reference path="./HtvModel.d.ts" />
/// <reference path="./DataHelper.d.ts" />

namespace Mq {

    export class PlayerData implements Mq.IPlayerData {
        public readonly layoutName: string;
        public readonly capabilities: number;
        public readonly viewCount: number;
        public readonly volume: number;
        public readonly geoPosition: Mq.GeoPosition;
        public readonly metadata: Mq.PlayerMetadata;
        public readonly layout: Mq.Layout;

        constructor(metadata: Mq.PlayerMetadata) {
            this.metadata = metadata;
            this.layoutName = metadata.playerId;
            this.capabilities = metadata.jsPlayerCapabilities || 0;
            this.viewCount = metadata.viewCount;
            this.volume = metadata.volume;
            this.geoPosition = metadata.geoPosition;
            this.layout = metadata.channelLayer;
        }

        private cachedViewMode: Mq.ViewMode;
        public get viewMode(): Mq.ViewMode {
            if (this.cachedViewMode)
                return this.cachedViewMode;

            if (this.metadata.thumbnailRecordingMode)
                return this.cachedViewMode = Mq.ViewMode.Thumbnail;

            if (this.metadata.viewMode == Mq.PlayerViewMode.Preview)
                return this.cachedViewMode = Mq.ViewMode.Preview;

            if (this.metadata.viewMode == Mq.PlayerViewMode.Edit)
                return this.cachedViewMode = Mq.ViewMode.Edit;

            if (this.metadata.__isDevHost)
                return this.cachedViewMode = Mq.ViewMode.Debug;

            // Q: What if we're on Android or other embedded Player?
            if (typeof window.navigator.userAgent === 'string' &&
                window.navigator.userAgent.indexOf(' mqCef/') >= 0)
                return this.cachedViewMode = Mq.ViewMode.LocalChannel;

            return this.cachedViewMode = Mq.ViewMode.WebChannel;
        }

        private cachedAttributes: Mq.StringMap<Mq.Attribute | Mq.StringMap<Mq.Attribute>>;
        public get attributes(): Readonly<Mq.StringMap<Mq.Attribute | Mq.StringMap<Mq.Attribute>>> {
            if (this.cachedAttributes)
                return this.cachedAttributes;

            if (!this.metadata || !this.metadata.attributes)
                return this.cachedAttributes = {};

            this.cachedAttributes = this.metadata.attributes.reduce((o, v) => {
                o[v.name] = v;
                return o;
            }, {});

            const sets: Mq.StringMap<Mq.StringMap<Mq.Attribute>> = {
                player: {},
                static: {},
                dyn: {},
                shared: {}
            };

            for (const attr of this.metadata.attributes) {
                const parts = attr.name.split('.');
                if (parts.length > 1) {
                    const coll = sets[parts[0]] || (sets[parts[0]] = {});
                    parts.shift();
                    coll[parts.join('.')] = attr;
                }
            }
            for (const key in sets)
                this.cachedAttributes[key] = sets[key];

            return this.cachedAttributes;
        }
    }

    export class PlaylistData implements Mq.IPlaylistData {
        public readonly isFirst: boolean;
        public readonly isLast: boolean;
        public readonly invocationCount: number = 0;
        public readonly priorityLevel: number = 0;

        constructor(metadata: Mq.PlayerMetadata) {
            this.isFirst = !!metadata.firstInPlaylist;
            this.isLast = !!metadata.lastInPlaylist;
            this.invocationCount = metadata.playlistInvocationCount || 0;
            this.priorityLevel = metadata.priorityLevel || 0;
        }
    }

    class TemplateData implements Mq.ITemplateData {
        constructor(metadata: Mq.PlayerMetadata) {
            this.settings = metadata.pqiTags ?? {};
        }
        public settings: Readonly<Mq.StringMap<any>>;
    }

    export class DataHelper implements Mq.IDataHelper {

        type: 'op' = 'op';

        constructor(
            public rawData: any,
            public playerMetadata: Mq.PlayerMetadata) {

            this.content = rawData;

            this.channel = rawData?.channel;
            this.client = playerMetadata.htv
                ? {
                    id: playerMetadata.htv.clientId,
                    channel: playerMetadata.htv.channel
                }
                : {
                    // Old or newer Player.
                    id: playerMetadata?.htvClientId || playerMetadata?.playbackDeviceId,
                    channel: rawData?.channel
                };
        }

        public content: any;

        // channel - (Mq.ChannelInfo)
        public readonly channel: Readonly<Mq.ChannelInfo>;
        // client
        //  id
        //  channel - (Mq.ChannelInfo)
        public readonly client: Readonly<Mq.IClientData>;

        private cachedTemplate: TemplateData;
        public get template(): Readonly<ITemplateData> {
            return this.cachedTemplate || (this.cachedTemplate = new TemplateData(this.playerMetadata));
        }

        private cachedPlayer: PlayerData;
        public get player(): Readonly<IPlayerData> {
            return this.cachedPlayer || (this.cachedPlayer = new PlayerData(this.playerMetadata));
        }

        // playlist
        //  isFirst
        //  isLast
        //  invocationCount
        //  priorityLevel
        private cachedPlaylist: PlaylistData;
        public get playlist(): Readonly<IPlaylistData> {
            return this.cachedPlaylist || (this.cachedPlaylist = new PlaylistData(this.playerMetadata));
        }

        public static isAttribute(v: any): v is Mq.Attribute {
            return v && (typeof v === 'object') && v.name && v.active && (typeof v.name === 'string') && (typeof v.active === 'boolean');
        }

        private static cachedHelper: DataHelper;
        private static cachedData: any;

        public static get(data: any, playerMetadata: Mq.PlayerMetadata): DataHelper {
            return DataHelper.cachedData !== data
                ? (DataHelper.cachedHelper = new DataHelper(DataHelper.cachedData = data, playerMetadata))
                : DataHelper.cachedHelper;
        }

        public static getNonCached(data: any, playerMetadata: Mq.PlayerMetadata): DataHelper {
            return DataHelper.cachedData !== data
                ? new DataHelper(data, playerMetadata)
                : DataHelper.cachedHelper;
        }

        getDuration(defaultSeconds: number = 10): number {
            const acceptMetadataDurationModes = [Mq.ViewMode.Edit, Mq.ViewMode.Debug];

            const res = this.player.metadata.duration ||
                (acceptMetadataDurationModes.some(vm => vm === this.player.viewMode) && this.content?.metadata?._duration) ||
                defaultSeconds;

            return Number(res);
        }
    }
}
