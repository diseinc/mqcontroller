/// <reference path="./HtvModel.d.ts" />
/// <reference path="./TemplateController.d.ts" />

declare namespace Mq {

    interface TemplateView {
        mqController?: Mq.ITemplateController;

        onPreload(data: Htv.ContentTemplateData,
            onReady: OnTemplateReadyCallback,
            onError: OnErrorCallback): void;
        onLoad(controller: Mq.ITemplateController,
            onReady?: () => void): void;
        onShift(counter: number, onEnd: (msg?: (string | Event | OnTemplateEndedReturn)) => void): void;
        onPlay(): void;
        onPause(): void;
        onMetadata(metadata: Mq.PlayerMetadata): void;
        onVolume(volume: number): void;
        onQueueNext(info: Mq.QueueInfo): void;
        onEditorMessage(message: any): void;
        onAttribute(attribute: Mq.UpdatedAttribute): void;
        onGeoPosition(geoCoords: Mq.GeoPosition): void;
    }
}
