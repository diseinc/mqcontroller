/// <reference path="./HtvModel.d.ts" />

declare namespace Mq {

    export const enum ViewMode {
        Edit = 'edit',
        Preview = 'preview',
        LocalChannel = 'local',
        WebChannel = 'server',
        Debug = 'debug',
        Thumbnail = 'thumb',
    }

    export interface IPlayerData {
        readonly layoutName: string;
        readonly capabilities: number;
        readonly volume: number;
        readonly geoPosition: Mq.GeoPosition;
        readonly layout: Readonly<Mq.Layout>;

        readonly metadata: Mq.PlayerMetadata;

        readonly viewMode: ViewMode;
        readonly attributes: Mq.StringMap<Mq.Attribute | Mq.StringMap<Mq.Attribute>>;
    }

    export interface IPlaylistData {
        readonly isFirst: boolean;
        readonly isLast: boolean;
        readonly invocationCount: number;
        readonly priorityLevel: number;
    }

    export interface ITemplateData {
        // ZZZ: Extends IContentData in Htv.
        settings: Readonly<Mq.StringMap<any>>;
    }

    export interface IClientData {
        id: string;
        channel: Mq.ChannelInfo;
    }

    export interface IDataHelper {

        type: 'op' | 'htv';
        channel: Readonly<Mq.ChannelInfo>;
        client: Readonly<IClientData>;

        content: Readonly<any>;

        template: Readonly<ITemplateData>;
        player: Readonly<IPlayerData>;
        playlist: Readonly<IPlaylistData>;

        getDuration(defaultSeconds?: number): number;
    }
}
