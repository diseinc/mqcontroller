/*****************************************************
   // Template player example usage.
   // This example will embed template "template.htm" and
   // play it in a loop.

   var player = new MqTemplatePlayer();

   var data = {data:[{"MediaL":{"type":"any","files":[{"url":"video1.mp4"}]}}]};

   // Fires when the embeded template is initiated
   player.onready = function() {
      player.preload(data);
   }

   // Show when preloaded
   player.onpreloadcomplete = function() {
      player.shift();
   }

   // Preload next item when ended (same item in this example)
   player.ontemplateended = function() {
      player.preload(data);
   }

   // Virtual template size and aspect ratio when scale=true
   player.width = 1920;
   player.height = 1080;
   player.scale = true;

   // Embed template in the .container element.
   // Template is scaled to fit the container and aspect ratio is preserved (when scale=true).
   // When template is initiated the onready callback is fired.
   player.embed('template.htm', document.querySelector('.container'));


*****************************************************/

const CORS_PROXY_URL: string = 'https://op.multiq.com/publiq/op/app_cors_proxy.php?url=';

namespace Mq
{
    // loadMode:
    //    'auto' = Both MqTemplate templates and regular URL:s are supported
    //    'template' = Template has to implement MqTemplate
    //    'noAPI' = Does not support templates.
    export type LoadMode = 'auto' | 'template' | 'noAPI';

    interface ResizeResult {
        origWidth: number;
        origHeight: number;
        newWidth: number;
        newHeight: number;
    }

    export class TemplatePlayer {
        element: HTMLIFrameElement = null;
        templateWindow: Window = null;
        container: HTMLElement = null;
        // Template resolution (when scale=true)
        width: number = 1920;
        height: number = 1080;
        displayScrollBars: boolean = false;

        preloadStartTime: number = 0;

        // Scale template?
        scale: boolean = true;

        // If template implements the api
        noApiMode: boolean = false;

        // If this class is allowed to log to the console
        logToConsole: boolean = false;

        // Token when communicating with the template
        token: string = Math.round(Math.random() * 1000000000).toString(36);

        // Preload counter
        itemCount: number = null;

        // If template is initiated
        apiReady: boolean = false;

        // If preload is complete and ready to shift()
        preloadComplete: boolean = false;

        // If shift event has been sent
        shiftSent = false;

        templateEnded = false;

        preloadInProgress = false;

        hardTimeout = 5000;

        templateInitTimeout = 500; // ms before fallback to noApiMode

        loadMode: LoadMode = 'auto';

        skipPreloadChecks: boolean = false;

        // timeoutNext = null;
        // currentRecord = null;
        // shiftOnPreloadComplete = false;
        // loop = false;

        constructor() {
            // Handle resize
            window.addEventListener('resize', (ev) => this.scaleElement());

            // Handle incoming messages from template
            window.addEventListener('message', (ev) => {
                // console.log(ev.data.event, ev.data);

                const msg = ev.data;

                // Check if message is addressed to us
                if (msg.token != this.token) {
                    return;
                }

                // Handle incoming
                switch (msg.event) {
                    case 'OnCmdReady':
                        // Save template window ref so we can sendTemplateEvent
                        this.templateWindow = ev.source as Window;
                        this.setTemplateReady();
                        break;

                    case 'OnPreloadComplete':
                        this.setPreloadComplete(msg.data);
                        break;

                    case 'OnPreloadError':
                        this.preloadComplete = false;
                        this.preloadInProgress = false;
                        this.log('Preload error: ' + msg.data.message);
                        this.dispatchEvent('preloaderror', [Error(msg.data.message)]);
                        break;

                    case 'OnTemplateEnded':
                        if (this.templateEnded) {
                            this.log('Got second template ended event. Event ignored.');
                            break;
                        }
                        this.templateEnded = true;
                        this.log('Template ended');
                        this.dispatchEvent('templateended');
                        break;

                    case 'OnLog':
                        // console.log('[Template:' + msg.data.level.toUpperCase() + ']: ' + msg.data.message);
                        this.dispatchEvent('log', [msg.data.message]);
                        break;

                    // Set global schedule attribute
                    case 'OnSetAttribute':
                        this.dispatchEvent('attribute', [msg.data]);
                        break;

                    // MqTemplate query interface. Mainly for the get/setPersistentItem methods.
                    case 'OnQuery':
                        this.handleQuery(msg.data);
                        break;
                }
            });

            this.log('MqTemplatePlayer initiated');
        }

        initVars(): void {
            this.element = null;
            this.templateWindow = null;
            this.container = null;
            this.scale = true;
            this.noApiMode = false;
            this.logToConsole = false;
            this.token = Math.round(Math.random() * 1000000000).toString(36);
            this.itemCount = null;
            this.apiReady = false;
            this.preloadComplete = false;
            this.shiftSent = false;
            this.templateEnded = false;
            this.preloadInProgress = false;
            this.templateInitTimeout = 500;
            this.hardTimeout = 5000;
            this.loadMode = 'auto';
            this.skipPreloadChecks = false;
        }

        /**
         * Supported events
         */
        public onready = null; // When template is initiated (cmdReady) or fell back to noApiMode.
        public onerror = null; // Failed to initiate
        public onlog = null; // Log message from MqTemplatePlayer or MqTemplate
        public onscale = null; // When template iframe is resized
        public onpreloadcomplete = null; // MqTemplate called .templateReady()
        public onpreloaderror = null; // MqTemplate called .templateError()
        public ontemplateended: (msg? : (string | Event)) => void = null; // MqTemplate called .templateEnder()
        public onattribute = null; // MqTemplate called .setAttribute()

        /**
         * Default key/value storage object for MqTemplate set/getPersistentItem.
         * Point this to another object when needed.
         */
        public persistentItems = {};

        /**
         * Dispatch on<eventName>
         */
        dispatchEvent(eventName: string, ...args: any): Boolean | void {

            if (!args)
                args = [];

            // Callback fn
            var cb = this['on' + eventName];
            if (typeof cb != 'function')
                return false;

            // Make callback
            return cb?.(...args);
        }

        /**
         * Log to console with prefix
         */
        log(msg: string): void {
            if (this.logToConsole) {
                console.log('[MqTemplatePlayer] ' + msg);
            }
            this.dispatchEvent('log', [ msg ]);
        }

        delay(ms: number, rejectOnComplete: boolean = false): Promise<void> {
            return new Promise((resolve, reject) => {
                setTimeout(() => rejectOnComplete ? reject() : resolve(), ms);
            });
        }

        /**
         * Embed template
         */
        embed(url: string, containerElement?: HTMLElement): HTMLElement {
            if (this.apiReady)
                this.destroy();

            const addQueryParam = (baseUrl: string, query: string): string => {
                return baseUrl + (baseUrl.indexOf('?') == -1 ? '?' : '&') + query;
            };

            const isMqCef = typeof navigator.userAgent == 'string' && navigator.userAgent.indexOf(' mqCef/') > -1;
            const tokenUrl =  this.loadMode === 'noAPI' ? url : addQueryParam(url, 'token=' + this.token);
            let checkUrl = addQueryParam(tokenUrl, 'checkUrl=1');

            if (!isMqCef)
                checkUrl = CORS_PROXY_URL + escape(checkUrl);

            // Create template iframe element
            const e = document.createElement('iframe') as HTMLIFrameElement;

            e.allow = 'camera *; microphone *; autoplay *';
            e.referrerPolicy = 'no-referrer';

            // Make sure template can't tamper parent
            e.setAttribute('sandbox', 'allow-scripts allow-forms allow-same-origin');

            e.style.width =
                e.style.height = '100%';
            e.style.border = '0';

            if (this.displayScrollBars) {
                e.style.overflow = 'auto';
            } else {
                e.style.overflow = 'hidden';
                // Deprecated in html, but seems to be the only option for now,
                // eventhough overflow - hidden should do the trick on its own:
                e.scrolling = 'no';
            }

            e.src = tokenUrl;

            // Save ref
            this.element = e;

            // Append to DOM if a container element was defined
            if (containerElement) {
                this.container = containerElement;

                // Insert element
                containerElement.appendChild(e);

                // Handle scaling
                this.scaleElement();
            }
            // else
                // The caller should embed the element manually, and then call elementAppended()

            const promises: Promise<void>[] = [];

            promises.push(new Promise((_, reject) => {
                // FF is probably the only browser that sometime fire error events
                e.onerror = () => reject(Error('Failed to load template iframe (onerror fired) loadMode=' + this.loadMode + ' url=' + url))
            }));

            if (this.skipPreloadChecks && this.loadMode === 'noAPI') {
                promises.push(Promise.resolve().then(() => {
                    this.noApiMode = true;
                }));
            }
            else {
                // Handle the case when iframe onload/onerror never fires.
                // This is the case for invalid https urls in Firefox 55 and "mixed content" urls in Chrome 61
                promises.push(this.delay(this.hardTimeout, true)
                    .catch(() => {
                        throw Error(
                            'Template iframe onload timed out after ' + this.hardTimeout + 'ms' +
                            ' loadMode=' + this.loadMode + ' url=' + url);
                    }));

                // Handle fallback to noApiMode (standard html page).
                // The onload event doesn't say if the load was successful. Just loaded :(

                const tmpFunc =
                    async (): Promise<void> => {

                        await new Promise<void>(resolve => e.onload = () => resolve());

                        if (this.apiReady || this.loadMode === 'noAPI') {
                            this.noApiMode = this.loadMode === 'noAPI';
                            return;
                        }

                        // Allow some time for the template to initiate before we fallback to noApiMode.
                        await this.delay(this.templateInitTimeout);

                        if (this.apiReady)
                            return;

                        switch (this.loadMode) {
                            // If loadMode=auto we don't know if
                            //   1) url failed to load or..
                            //   2) template api is not supported
                            case "auto":
                                // If a standard browser (not mqCef, as mqCef allows cross-origin requests)
                                // As we don't know the request failed we have to treat it as if the url
                                // was found (or all external urls would fail for standard browsers).

                                if (!isMqCef ||
                                    // Perform XHR Head request to check if url exists
                                    await this.checkUrl(checkUrl)
                                    .catch(err => {
                                        if (this.apiReady)
                                            return;

                                        throw err;
                                    })) {

                                    this.noApiMode = !this.apiReady;
                                    return;
                                }

                                throw Error('Failed XHR-loading url');

                            case "template":
                                throw Error('Template did not initiate loadMode=template url=' + url);
                            //case 'noAPI': // Not relevant
                            //    break;
                        }
                    };

                promises.push(tmpFunc());
            }

            let resultHandled: boolean = false;

            // Promise.race is weird in that it can fire more than once.
            // First for a result, then if another promise rejects...
            // This may be a bug in the polyfill, though.
            Promise.race(promises)
                .then(() => {
                    resultHandled = true;
                    this.setTemplateReady();
                })
                .catch((ex) => {
                    if (!resultHandled)
                        this.setTemplateError(ex);
                });

            // The caller should embed the element manually, and then call elementAppended()
            return this.element;
        }

        /**
         * If embed() was called without a container element,
         * call this method when element has been appended to DOM manually.
         */
        elementAppended(): void {
            // Assume container is the parent node
            this.container = this.element.parentNode as HTMLElement;

            // Handle scaling
            this.scaleElement();
        }

        /**
         * Destroy embeded template
         */
        destroy(): void {
            if (this.element) {
                this.container.removeChild(this.element);
                this.element.onload = null;
                this.element.onerror = null;
                this.element = null;
                this.container = null;
            }

            this.initVars();
        }



        /**
         * Set template ready
         */
        setTemplateReady(): void {
            if (this.apiReady) {
                // Already initialized
                return;
            }

            this.element.onload = null;

            this.log('Template ready. API support: ' + (this.noApiMode ? 'No' : 'Yes'));

            // Template is ready to recieve events
            this.apiReady = true;

            this.dispatchEvent('ready');
        }

        /**
         * Set template load error
         */
        setTemplateError(err: any): void {
            this.element.onload = null;
            this.element.onerror = null;
            this.dispatchEvent('error', [err]);
        }

        /**
         * Set preload complete.
         * Preload properties can be supplied by the template in the preloadComplete() call
         */
        setPreloadComplete(properties?: any): void {
            const took = new Date().getTime() - this.preloadStartTime;
            this.log('Preload complete took=' + took + 'ms');
            this.preloadComplete = true;
            this.preloadInProgress = false;
            this.shiftSent = false;

            var duration = null;
            var suggestedNextPreload = null;
            var rejectPlay = false;
            if (Object.prototype.toString.call(properties) == '[object Object]') {
                duration = parseFloat(properties.duration);
                if (isNaN(duration)) {
                    duration = null;
                }
                suggestedNextPreload = parseFloat(properties.suggestedNextPreload);
                if (isNaN(suggestedNextPreload)) {
                    suggestedNextPreload = null;
                }
                // Preload ok (no error), but skip this item
                rejectPlay = Boolean(properties.rejectPlay);
            }
            this.dispatchEvent('preloadcomplete', [{
                duration: duration,
                suggestedNextPreload: suggestedNextPreload,
                rejectPlay: rejectPlay,
                took: took,
            }]);
        }

        /**
         * Preload next item
         */
        preload(data: any, metadata: any): void {
            if (this.preloadInProgress)
                throw Error('Preload already in progress');

            // Flag is reset by preload complete or preload error
            this.preloadInProgress = true;

            if (!data || typeof data !== 'object')
                data = {};

            // Increase itemCount
            if (this.itemCount === null)
                this.itemCount = 0;
            else
                this.itemCount++;

            this.preloadComplete = false;
            this.preloadStartTime = new Date().getTime();

            const defaultMetadata = {
                jsPlayerCapabilities: null,
                viewCount: 0,
                volume: 1,
                lastQueueNumber: false,
                geoPosition: {
                    valid: false,
                    longitude: null,
                    latitude: null,
                },
                attributes: [],
            };

            let meta;

            meta = (typeof Object.assign === 'function')
                ? Object.assign(defaultMetadata, metadata)
                // IE does not support Object.assign...
                : (metadata ?? defaultMetadata);

            // Send metadata event
            this.sendTemplateEvent('OnMetadata', meta);

            this.log('Preload item=' + this.itemCount + ' data=' + JSON.stringify(data));

            // Send preload event
            this.sendTemplateEvent('OnPreload', JSON.stringify(data));
        }

        /**
         * Post event to template
         */
        sendTemplateEvent(event: any, data: any): void {
            if (!this.templateWindow && !this.noApiMode)
                throw Error('Can\'t post message to template. Template window ref not available');

            if (this.noApiMode) {
                // Simulate api response
                switch (event) {
                    // OnPreload response should be preload complete
                    case 'OnPreload':
                        setTimeout(() => this.setPreloadComplete());
                        break;
                    default:
                        break;
                }
            }
            else {
                // Post event to template
                this.templateWindow.postMessage({
                    type: "jsPlayerEvent",
                    event: event,
                    data: data,
                    token: this.token,
                }, '*');
            }
        }

        /**
         * Shift template
         */
        shift(): void {
            if (!this.preloadComplete)
                throw Error('Can\'t shift - Preload not complete');
            if (this.shiftSent)
                throw Error('Can\'t shift - Shift event already sent');

            this.log('Shift template item=' + this.itemCount);

            this.templateEnded = false;
            this.setVolume(0);
            this.sendTemplateEvent('OnShift', null);
        }

        /**
         * Send pause event
         */
        pause(): void {
            this.sendTemplateEvent('OnPause', null);
        }

        /**
         * Scale iframe element to fit container and maintain aspect ratio
         */
        scaleElement(): void {
            if (!this.element) {
                return;
            }

            const cWidth = this.container.clientWidth;
            const cHeight = this.container.clientHeight;

            // If scaling is disabled
            if (!this.scale) {
                // Fill parent container
                this.element.style.width = '100%';
                this.element.style.height = '100%';

                this.dispatchEvent('scale', [{
                    scaleFactor: 1,
                    containerWidth: cWidth,
                    containerHeight: cHeight,
                    elementWidth: this.element.clientWidth,
                    elementHeight: this.element.clientHeight,
                }]);

                return;
            }

            // Calculate new size that fits the stage
            const dim = this.calcResize(this.width, this.height, cWidth, cHeight) as ResizeResult;

            // Calculate scale factor for the css2d scale transform
            const scaleFactor = Math.min(dim.newWidth / this.width, dim.newHeight / this.height);

            const style = this.element.style;

            // Set requested template resolution
            style.width = this.width + 'px';
            style.height = this.height + 'px';

            // Scale to fit
            style.transform = 'scale(' + scaleFactor + ') translate(-50%, -50%)';
            style.transformOrigin = 'top left';
            style.left = '50%';
            style.top = '50%';
            // Absolute positioning is required to keep the container size intact
            // style.position = 'relative';
            style.position = 'absolute';

            this.dispatchEvent('scale', [{
                scaleFactor: scaleFactor,
                containerWidth: cWidth,
                containerHeight: cHeight,
                elementWidth: dim.newWidth,
                elementHeight: dim.newHeight,
            }]);
        }

        /**
         * Calculate new dimensions while keeping proportions.
         * Copy of the php-function with the same name
         *
         * Examples:
         *    calcResize(16, 9, 32, 0) // newHeight=18
         *    calcResize(16, 9, 0, 18) // newWidth=32
         *    calcResize(16, 9, 32,32) // Fit bounding box 32x32 newWidth=32 newHeight=18
         */
        calcResize(origWidth: number, origHeight: number, newWidth: number, newHeight: number): (boolean | ResizeResult) {
            if (origWidth == 0 || origHeight == 0 || (newWidth == 0 && newHeight == 0)) {
                return false;
            }
            if (newWidth != 0 && newHeight != 0) {
                // Fit bounding box
                const res = this.calcResize(origWidth, origHeight, newWidth, 0);
                if (typeof res !== 'boolean' && res.newHeight > newHeight) {
                    return this.calcResize(origWidth, origHeight, 0, newHeight);
                }
                return res;
            }
            else if (newWidth == 0) {
                newWidth = (origWidth / origHeight) * newHeight;
            }
            else {
                newHeight = newWidth / (origWidth / origHeight);
            }
            return {
                origWidth: origWidth,
                origHeight: origHeight,
                newWidth: newWidth,
                newHeight: newHeight,
            };
        }

        /**
         * Set template volume
         */
        setVolume(volume: number): void {
            // This should trigger MqTemplate::onVolume
            this.sendTemplateEvent('OnVolume', { volume: volume });
        }

        /**
         * Check url by performing a HEAD request
         */
        async checkUrl(url: string, timeout: number = 3000): Promise<boolean> {

            try {
                const response = await Promise.race([
                    this.delay(timeout),
                    fetch(url, {
                        method: 'HEAD',
                        mode: 'no-cors'
                    })]);

                return response ? response.ok : false;
            } catch { }

            return false;
        }

        /**
         * Handle query from template
         */
        handleQuery(query: any): void {
            // Perform onquery callback.
            // Callback should return true to prevent the default action
            const res = this.dispatchEvent('query', [query.name, query.params, (result, response) => {
                this.sendTemplateEvent('OnQueryResponse', {
                    id: query.id,
                    result: result,
                    response: response
                });
            }]);

            // If onquery callback handled the event
            if (res === true) {
                return;
            }

            // Default handlers
            switch (query.name) {
                // Get persistent item
                case 'getPersistentItem':
                    if (query.params.key in this.persistentItems) {
                        this.sendTemplateEvent('OnQueryResponse', {
                            id: query.id,
                            result: true,
                            response: this.persistentItems[query.params.key]
                        });
                    }
                    else {
                        this.sendTemplateEvent('OnQueryResponse', {
                            id: query.id,
                            result: false,
                            response: false,
                        });
                    }
                    break;

                // Set persistent item
                case 'setPersistentItem':
                    this.persistentItems[query.params.key] = query.params.value;
                    this.sendTemplateEvent('OnQueryResponse', {
                        id: query.id,
                        result: true,
                        response: this.persistentItems[query.key]
                    });
                    break;

                default:
                    this.log('Unknown query.name=' + query.name);
            }
        }
    }
}
