/// <reference path="./HtvModel.d.ts" />

declare namespace Mq {

    type OnTemplateReadySingleParam = (Mq.OnPreloadReadyReturn | number | boolean);
    type OnTemplateReadyCallback = (objOrSuggestedPreloadOrRejectPlay?: OnTemplateReadySingleParam, duration?: number, rejectPlay?: boolean) => void;
    type OnErrorCallback = (message?: any) => void;

    interface ITemplateController {
        readonly apiVersion: number;
        readonly classVersion: number;

        templateEnded(message?: (string | Event | OnTemplateEndedReturn)): void;
        setAttribute(attribute: Mq.Attribute, force?: boolean): any;

        editorMessage(message: any): any;
        log(message: string | any, level?: LogLevels): void;

        setGlobalProofExtraData(data: any): any;
        logProofOfPlay(params: ProofOfPlayLogEntry): void;

        setPersistentItem(key: string, val: any): void;
        getPersistentItem(key: string, defaultValue?: any): any;

        getPersistentChannelItem<T>(key: string, callback: (res: T) => void, defaultValue?: T): void;
        setPersistentChannelItem(key: string, value: any, callback?: (res: { result: boolean; error?: Error }) => void);

        triggerScreenshot(): void;
    }
}
