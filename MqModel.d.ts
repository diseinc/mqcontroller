
declare namespace Mq {
    interface StringMap<T> {
        [K: string]: T;
    }

    type TemplateSettings = StringMap<any>

    type DataModelFormats = 'htv' | 'op';

    interface OnCmdReadyReturn {
        apiVersion: number;
        classVersion: number;
        format?: DataModelFormats
    }

    type OnLoadCompleteReturn = DataModelFormats | void | boolean;

    interface CommonReturn {
        proofExtraData?: any;
    }
    interface OnPreloadReadyReturn extends CommonReturn {
        duration?: number;
        suggestedNextPreload?: number;

        // In edit-mode, the template can inform the editor of its duration-restrictions
        // (causing the value in the UI to be updated). If both are set to the same value,
        // the time-picker control in the UI will have user-input disabled.
        // Ignored by jsPlayer.
        cms?: {
            minDuration?: number;
            maxDuration?: number;
        };

        rejectPlay?: boolean;
        hasAudio?: boolean;
        useCustomScreenshotEvent?: boolean;
        screenshotTime?: number;

        reuseTemplate?: boolean;
        scaling?: true;
        /** In the syntax [width]x[height] e.g. 1920x1080 */
        resolution?: string;
    }

    interface OnTemplateEndedReturn extends CommonReturn {
        message?: string;
        forceNew?: boolean;
    }

    interface OnPreloadErrorReturn {
        message?: string;
    }

    const enum LogLevels {
        debug = 'debug',
        info = 'info',
        warning = 'warn',
        error = 'error'
    }

    interface ProofOfPlayLogEntry {
        id: string;
        duration?: number;
        segmentId?: string;
        file?: string;
    }

    interface LayerProperties {
        top: number;
        left: number;
        width: number;
        height: number;
        background: string; // style.background
        pointerEvents: boolean;
        scale: boolean;
        visible: boolean;
    }

    interface Attribute {
        name: string;
        active: boolean;
        data?: string;
        label?: string;
        shared?: boolean;
    }

    interface UpdatedAttribute extends Attribute {
        force?: boolean;
        origin?: string;
        master?: string;
        type?: string;
        revision?: number;
        changed?: Array<keyof Attribute>;
    }

    // ZZZ: Coordinates does not exist anymore? Have to investigate later...
    interface ObsoleteDOMCoordinates {
        readonly accuracy: number;
        readonly altitude: number | null;
        readonly altitudeAccuracy: number | null;
        readonly heading: number | null;
        readonly latitude: number;
        readonly longitude: number;
        readonly speed: number | null;
    }

    // ZZZ: used to be "extends Partial<Coordinates>"
    interface GeoPosition extends Partial<ObsoleteDOMCoordinates> {
        valid?: boolean;
        numberSatellites?: number;
    }

    interface ChannelInfo {
        id: string;
        name: string;
        lastModified: string;
    }

    export const enum PlayerViewMode {
        Edit = 'edit',
        Preview = 'preview'
    }

    interface QueueInfo {
        desk: number;
        line: string;
        number: number;
        metadata?: {
            servicetype: number,
            tickettype: number,
            zeros: number,
        };
    }

    interface Layout {
        type?: 'player' | 'slavePlayer';
        mode?: 'normal' | 'dataPlaylist';
        top?: number;
        left?: number;
        width?: number;
        height?: number;
        //layer?: number;
        label?: string;
    }

    interface HtvSpecific {
        channel?: ChannelInfo;
        clientId?: string;
    }

    interface PlayerMetadata {
        jsPlayerCapabilities?: number;
        viewCount: number;
        viewMode?: PlayerViewMode;
        volume: number;
        groupRelativePath: string;
        lastQueueNumber: any;
        pqiTags: TemplateSettings;
        attributes: Mq.Attribute[];
        geoPosition: Mq.GeoPosition;
        firstInPlaylist?: boolean;
        lastInPlaylist?: boolean;
        playlistInvocationCount?: number;
        priorityLevel?: number;
        playerId?: string;// Layout-id (scene)
        // Obsolete, moved to htv.clientId:
        htvClientId?: string;
        htv?: Mq.HtvSpecific;
        thumbnailRecordingMode?: boolean;

        syncGroup?: {
            role: 'slave' | 'master';
            masterHostInstanceId: string;
            slaveHostInstanceId: string;
            masterPlayerId: string;
        }

        duration?: number;
        playbackDeviceId?: string; // For OP.

        channelLayer?: Layout;

        __isDevHost?: boolean;
    }
}
