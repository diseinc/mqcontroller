/// <reference path="./HtvModel.d.ts" />
/// <reference path="./DataHelper.d.ts" />

namespace Htv {

    export namespace DataHelperData {
        interface FieldValues<T> {
            [key: string]: T;
        }
        interface FieldValuesNumIndex<T> {
            [index: number]: FieldValues<T>;
        }
        type FieldValuesBase<T> = FieldValues<T> & FieldValuesNumIndex<T>;

        function filterByFieldType<T>(arrayOfFieldMaps: Mq.StringMap<Htv.OtherField | Htv.FilesField>[],
            fieldTypes: Htv.FieldTypes[], transform?: (v: any) => T): FieldValuesBase<T>;
        function filterByFieldType<T>(arrayOfFieldMaps: Mq.StringMap<Htv.OtherField | Htv.FilesField>[],
            fieldTypes: Htv.FieldTypes, transform?: (v: any) => T): FieldValuesBase<T>;
        function filterByFieldType<T>(arrayOfFieldMaps: Mq.StringMap<Htv.OtherField | Htv.FilesField>[],
            fieldSelector: (v: (Htv.OtherField | Htv.FilesField)) => boolean, transform?: (v: any) => T): FieldValuesBase<T>;
        function filterByFieldType<T>(
            arrayOfFieldMaps: Mq.StringMap<Htv.OtherField | Htv.FilesField>[],
            fieldTypesOrSelector: Htv.FieldTypes | Htv.FieldTypes[] | ((v: (Htv.OtherField | Htv.FilesField)) => boolean),
            transform?: (v: any) => T): FieldValuesBase<T> {

            if (!arrayOfFieldMaps)
                return {};

            let includeField: ((v: (Htv.OtherField | Htv.FilesField)) => boolean);

            if (typeof fieldTypesOrSelector === 'function')
                includeField = fieldTypesOrSelector;
            else if (Array.isArray(fieldTypesOrSelector))
                includeField = (f) => (<Htv.FieldTypes[]>fieldTypesOrSelector).some((v) => f.type === v);
            else
                includeField = (f) => (<Htv.FieldTypes>fieldTypesOrSelector) == f.type;

            const subset = arrayOfFieldMaps.reduce((a, v) => {
                let ia = {};
                // tslint:disable-next-line:forin
                for (const key in v) {
                    const f = v[key];

                    if (includeField(f)) {
                        if (!DataHelper.isMediaField(f)) {
                            if (f.value != null)
                                ia[key] = transform ? transform(f.value) : f.value;
                        }
                        else
                            ia[key] = (f.files && f.files.length > 0) ? f.files : void 0;
                    }
                }
                a.push(ia);
                return a;
            }, []);

            const res = {};
            if (subset.length > 0) {
                for (const key in subset[0])
                    res[key] = subset[0][key];
                for (let i = 0; i < subset.length; i++)
                    res[i] = subset[i];
            }

            return res;
        }

        // ZZZ: Needs interface etc.
        export class ContentData {
            public readonly id: string;
            public readonly name: string;
            public readonly duration: number | undefined;
            public readonly fields: Readonly<Array<Mq.StringMap<FieldValue>>>;
            public readonly metadata: Readonly<Mq.StringMap<any>>;

            constructor(protected _data: Htv.BaseContentData) {
                this.id = _data.id;
                this.name = _data.name;
                this.fields = _data.data;
                this.duration = _data.duration != null ? _data.duration : undefined;
                this.metadata = _data.metadata ? _data.metadata : {};
            }

            private cachedTextFieldValues: FieldValuesBase<string>;
            public get texts(): Readonly<FieldValuesBase<string>> {
                return this.cachedTextFieldValues ||
                    (this.cachedTextFieldValues = filterByFieldType<string>(this._data.data, Htv.FieldTypes.Text, String));
            }
            private cachedXmlFieldValues: FieldValuesBase<string>;
            public get xml(): Readonly<FieldValuesBase<string>> {
                return this.cachedXmlFieldValues ||
                    (this.cachedXmlFieldValues = filterByFieldType<string>(this._data.data, Htv.FieldTypes.XmlData, String));
            }
            private cachedGeodataFieldValues: FieldValuesBase<string>;
            public get geodata(): Readonly<FieldValuesBase<string>> {
                return this.cachedGeodataFieldValues ||
                    (this.cachedGeodataFieldValues = filterByFieldType<string>(this._data.data, Htv.FieldTypes.GeoData, String));
            }
            private cachedNumberFieldValues: FieldValuesBase<number>;
            public get numbers(): Readonly<FieldValuesBase<number>> {
                return this.cachedNumberFieldValues ||
                    (this.cachedNumberFieldValues = filterByFieldType<number>(this._data.data,
                        [Htv.FieldTypes.Number,
                        Htv.FieldTypes.Integer,
                        Htv.FieldTypes.Float,
                        Htv.FieldTypes.Double], Number));
            }
            private cachedEnumFieldValues: FieldValuesBase<number>;
            public get enums(): Readonly<FieldValuesBase<number>> {
                return this.cachedEnumFieldValues ||
                    (this.cachedEnumFieldValues = filterByFieldType<number>(this._data.data, Htv.FieldTypes.Enum, Number));
            }
            private cachedEnumStringFieldValues: FieldValuesBase<string>;
            public get enumStrings(): Readonly<FieldValuesBase<string>> {
                return this.cachedEnumStringFieldValues ||
                    (this.cachedEnumStringFieldValues = filterByFieldType<string>(this._data.data, Htv.FieldTypes.Enum, String));
            }
            private cachedmediaFieldValues: FieldValuesBase<Htv.ContentFile[]>;
            public get medias(): Readonly<FieldValuesBase<Htv.ContentFile[]>> {
                return this.cachedmediaFieldValues ||
                    (this.cachedmediaFieldValues = filterByFieldType<Htv.ContentFile[]>(this._data.data, Htv.FieldTypes.Files));
            }
            private cachedNonMediaFieldValues: FieldValuesBase<string>;
            public get nonMedia(): Readonly<FieldValuesBase<string>> {
                return this.cachedNonMediaFieldValues ||
                    (this.cachedNonMediaFieldValues =
                        filterByFieldType<string>(this._data.data, (f) => f.type != Htv.FieldTypes.Files, String));
            }

            private static isDateTime = /^\w+ ?\d{1,2}, *\d{4}, *\d{1,2}:\d{2}[\d:\.]* *(am|pm)?$/;
            private static isIsoDateTime = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}(?::\d{2}(?:\.\d{1,3})?)?$/;
            private static isIsoDateTimeIsh = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}(?::\d{2}(?:\.\d{1,3})?)?$/;

            private static isDate = /^\w+ ?\d{1,2}, *\d{4}$/;
            private static isIsoDate = /^\d{4}-\d{2}-\d{2}$/;

            private static isAmPmTime = /^\d{1,2}:\d{2}[\d:\.]* *(am|pm)$/;
            private static isTime = /^\d{2}:\d{2}(?::\d{2}(?:\.\d{1,3})?)?$/;

            private static parseDateTime(s: string): Date {
                if (this.isIsoDateTimeIsh.test(s))
                    s = s.replace(' ', 'T');

                if (!this.isIsoDateTime.test(s) && !this.isDateTime.test(s)) {
                    if (this.isTime.test(s))
                        s = '1970-01-01T' + s;
                    else if (this.isAmPmTime.test(s))
                        s = 'Jan 1, 1970, ' + s;
                    else if (this.isIsoDate.test(s))
                        s += 'T00:00:00';
                    else if (this.isDate.test(s))
                        s += ', 12:00 am';
                }

                return new Date(s);
            }

            private cachedDateTimeFieldValues: FieldValuesBase<Date>;
            public get dateTimes(): Readonly<FieldValuesBase<Date>> {
                return this.cachedDateTimeFieldValues ||
                    (this.cachedDateTimeFieldValues = filterByFieldType<Date>(this._data.data, Htv.FieldTypes.DateTime, (v) => {
                        const d = ContentData.parseDateTime(v);

                        return (d && !isNaN(d.getTime())) ? d : void 0;
                    }));
            }
            private cachedDateFieldValues: FieldValuesBase<Date>;
            public get dates(): Readonly<FieldValuesBase<Date>> {
                return this.cachedDateFieldValues ||
                    (this.cachedDateFieldValues = filterByFieldType<Date>(this._data.data, Htv.FieldTypes.Date, (v) => {
                        const d = ContentData.parseDateTime(v);

                        if (d && !isNaN(d.getTime())) {
                            d.setHours(0, 0, 0, 0);
                            return d;
                        }

                        return void 0;
                    }));
            }
            private cachedTimeFieldValues: FieldValuesBase<Date>;
            public get times(): Readonly<FieldValuesBase<Date>> {
                return this.cachedTimeFieldValues ||
                    (this.cachedTimeFieldValues = filterByFieldType<Date>(this._data.data, Htv.FieldTypes.DateTime, (v) => {
                        const d = ContentData.parseDateTime(v);

                        if (d && !isNaN(d.getTime())) {
                            d.setFullYear(1970, 0, 1);
                            return d;
                        }

                        return void 0;
                    }));
            }
        }

        export class TemplateData extends ContentData implements Mq.ITemplateData {
            constructor(data: Htv.BaseContentData, settings: Mq.TemplateSettings) {
                super(data);
                this.settings = settings ? settings : {};
            }
            public settings: Readonly<Mq.StringMap<any>>;
        }
    }
    export class DataHelper implements Mq.IDataHelper {

        type: 'htv' = 'htv';

        constructor(public rawData: Htv.ContentTemplateData, playerMetadata?: Mq.PlayerMetadata) {
            if (playerMetadata)
                rawData.__playerMetadata = playerMetadata;
            this.channel = rawData?.channel;
            this.client = rawData.__playerMetadata?.htv
                ? {
                    id: rawData.__playerMetadata?.htv?.clientId,
                    channel: rawData.__playerMetadata?.htv?.channel
                }
                : {
                    // Old Player.
                    id: rawData.__playerMetadata?.htvClientId || rawData.__playerMetadata?.playbackDeviceId,
                    channel: rawData?.channel
                };
        }

        // channel - (Mq.ChannelInfo)
        public readonly channel: Readonly<Mq.ChannelInfo>;
        // client
        //  id
        //  channel - (Mq.ChannelInfo)
        public readonly client: Readonly<Mq.IClientData>;

        private cachedContent: Htv.DataHelperData.ContentData;
        public get content(): Readonly<Htv.DataHelperData.ContentData> {
            return this.cachedContent || (this.cachedContent = new Htv.DataHelperData.ContentData(this.rawData));
        }

        private cachedTemplate: Htv.DataHelperData.TemplateData;
        public get template(): Readonly<Htv.DataHelperData.TemplateData> {
            return this.cachedTemplate || (this.cachedTemplate = new Htv.DataHelperData.TemplateData(this.rawData.template, this.rawData.settings));
        }

        private cachedPlayer: Mq.PlayerData;
        public get player(): Readonly<Mq.PlayerData> {
            return this.cachedPlayer || (this.cachedPlayer = new Mq.PlayerData(this.rawData.__playerMetadata));
        }

        // playlist
        //  isFirst
        //  isLast
        //  invocationCount
        //  priorityLevel
        private cachedPlaylist: Mq.PlaylistData;
        public get playlist(): Readonly<Mq.PlaylistData> {
            return this.cachedPlaylist || (this.cachedPlaylist = new Mq.PlaylistData(this.rawData.__playerMetadata));
        }

        public static isAttribute(v: any): v is Mq.Attribute {
            return v && (typeof v === 'object') && v.name && v.active && (typeof v.name === 'string') && (typeof v.active === 'boolean');
        }

        public static isOtherField(f: Htv.FieldValue): f is Htv.OtherField {
            return f && (f.type !== 'files');
        }
        public static isMediaField(f: Htv.FieldValue): f is Htv.FilesField {
            return f && (f.type === 'files');
        }

        private static cachedHelper: DataHelper;
        private static cachedData: Htv.ContentTemplateData;

        public static get(data: Htv.ContentTemplateData, playerMetadata?: Mq.PlayerMetadata): DataHelper {
            return DataHelper.cachedData !== data
                ? (DataHelper.cachedHelper = new DataHelper(DataHelper.cachedData = data, playerMetadata))
                : DataHelper.cachedHelper;
        }

        public static getNonCached(data: Htv.ContentTemplateData, playerMetadata?: Mq.PlayerMetadata): DataHelper {
            return DataHelper.cachedData !== data
                ? new DataHelper(data, playerMetadata)
                : DataHelper.cachedHelper;
        }

        getDuration(defaultSeconds: number = 10): number {
            return Number(
                this.rawData.duration ||
                this.rawData.template.duration ||
                (this.player.metadata.duration != 600
                    ? this.player.metadata.duration : null) ||
                defaultSeconds);
        }

        getExpiry(defaultExpiryMinutes: number = 0): Date {
            try {

                let expiryMinutes = this.template.numbers.ExpiryMinutes;

                if (Number(expiryMinutes) <= 0 && defaultExpiryMinutes > 0)
                    expiryMinutes = defaultExpiryMinutes;

                if (Number(expiryMinutes) > 0)
                    return new Date(new Date(this.content.metadata.created).getTime() + Number(expiryMinutes) * 60000);
            }
            catch { }

            return new Date(3000, 0, 1);
        }

        getFieldValue(fieldName: string, defaultVal?: any, index: number = 0): any {
            try {
                if (this.rawData && this.rawData.data && this.rawData.data[index]) {
                    let field = this.rawData.data[index][fieldName];

                    if (DataHelper.isOtherField(field))
                        return field.value;
                    else if (DataHelper.isMediaField(field)) {
                        let files = this.getMediaFieldFiles(fieldName, index);

                        if (files && files.length > 0)
                            return files[0].url;
                    }
                }
            }
            catch { }

            return defaultVal;
        }

        getMediaFieldFiles(fieldName: string, index: number = 0): Htv.ContentFile[] {
            try {
                if (this.rawData && this.rawData.data && this.rawData.data[index]) {
                    let field = this.rawData.data[index][fieldName];

                    if (DataHelper.isMediaField(field))
                        return field.files;
                }
            }
            catch { }

            return [];
        }
        getSettingValue(settingName: string, defaultVal?: any): any {
            if (this.rawData && this.rawData.settings) {
                let val = this.rawData.settings[settingName];

                return 'undefined' === typeof val ? defaultVal : val;
            }

            return defaultVal;
        }
    }
}
