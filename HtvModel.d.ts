/// <reference path="./MqModel.d.ts" />

declare namespace Htv {

    const enum FieldTypes {
        Text = 'text',
        Number = 'number',
        Integer = 'integer',
        Float = 'float',
        Double = 'double',
        Enum = 'enum',
        GeoData = 'geodata',
        XmlData = 'xmlData',
        Files = 'files',
        DateTime = 'dateTime',
        Date = 'date',
        Time = 'time'
    }

    interface ContentFile {
        url: string;
        name: string;
        properties: Mq.StringMap<any>;
    }
    interface Field {
        type: FieldTypes;
    }
    interface FilesField extends Field {
        files: ContentFile[];
    }
    interface OtherField extends Field {
        value: any;
    }

    type FieldValue = FilesField | OtherField;

    interface BaseContentData {
        id: string;
        name: string;
        duration: number;
        metadata: Mq.StringMap<any>;
        data: Array<Mq.StringMap<FieldValue>>;
    }
    interface TemplateData extends BaseContentData {
    }
    interface ContentTemplateData extends BaseContentData {
        template: TemplateData;
        settings: Mq.TemplateSettings;
        channel?: Mq.ChannelInfo;
        // Not part of standard jsPlayer data, added by controller from onMetadata:
        __playerMetadata?: Mq.PlayerMetadata;
    }
}
