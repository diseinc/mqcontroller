/// <reference path="./TemplateController.d.ts" />
/// <reference path="./TemplateView.d.ts" />

namespace Mq {

    const enum PlayState {
        Playing,
        Paused
    }

    interface PlayerQuery {
        id: string;
        hasBeenHandled?: boolean;
        name: string;
        params: any;
        timeoutHandle: any;
        callback?: (success: boolean, res: any) => void;
    }

    /**
     * Template controller
     */
    export class TemplateController implements ITemplateController {
        private _view: TemplateView = null;

        private currentMetadata: Mq.PlayerMetadata = null;
        currentData: any = null;

        private suggestedNextPreload: number = null;

        private _playState: PlayState = PlayState.Playing;

        private _queries: Mq.StringMap<PlayerQuery> = {};
        private onShiftCount: number = 0;
        private token: string;
        private parentWindow: Window;

        // Version of the jsPlayer <> MqTemplate API
        public readonly apiVersion: number = 9;
        // MqTemplate version
        // Version history:
        //    3 : 2015-??-??
        //    4 : 2015-12-17 - added onQueueNext, added metadata.groupRelativePath and metadata.lastQueueNumber
        //    5 : 2016-11-08 - added onEditorMessage, editorMessage
        //    6 : 2017-06-29 - metadata.attributes, metadata.jsPlayerCapabilities, onAttribute, setAttribute, onGeoPosition,
        //                     preloadComplete params duration, rejectPlay and suggestedNextPreload
        //    7 : 2017-06-30 - query api: setPersistentItem, getPersistentItem
        //    8 : 2019-03-06 - added setGlobalProofExtraData
        //    9 : 2020-08-14 - added logProofOfPlay
        public readonly classVersion: number = 9;

        constructor(view?: TemplateView, private minimizeLogNoise: boolean = false) {

            this._view = view ? view : <TemplateView>{};
            
            if ((<TemplateView>view).mqController !== undefined)
                (<TemplateView>view).mqController = this;

            this.parentWindow = window !== window.parent ? window.parent : null;
            // Get event token from get param token=
            this.token = window.location.search.match(/[?&]token=([^&#]+)/)?.[1] || 'default-token';

            // Listen to messages posted by parent window
            window.addEventListener('message', (ev: MessageEvent) => {
                let message = ev.data;

                if (!message || typeof message !== 'object') {
                    return;
                }

                // Should be a jsPlayerEvent
                if (message.type !== 'jsPlayerEvent') {
                    if (!this.minimizeLogNoise)
                        this.debug('Event ignored:  event=' + message.event + ' type="' + message.type + '"');
                    return;
                }

                // Handle events
                switch (message.event) {
                    // Metadata from player
                    case 'OnMetadata':
                        this.onMetadata(message.data);
                        break;

                    // Preload data
                    case 'OnPreload':
                        let parsedData: any = null;
                        try {
                            // Call the onParse method. Default action is JSON.parse
                            parsedData = this.parseData(message.data);
                        }
                        catch (err) {
                            // Failed to parse - can't continue
                            this.preloadError('onParse failed: ' + err.message + ' data: ' + message.data);
                            break; // Note: early break
                        }
                        this.onPreload(parsedData);
                        break;

                    // Show/shift template
                    case 'OnShift':
                        // Default playState after OnShift event is playing
                        this._playState = PlayState.Playing;
                        this.onShift();
                        break;

                    // Play template (if not already playing)
                    case 'OnPlay':
                        switch (this._playState)
                        {
                            case PlayState.Playing:
                                this.debug('OnPlay event ignored (already playing)');
                                break;

                            default:
                                this._playState = PlayState.Playing;
                                this.onPlay();
                                break;
                        }
                        break;

                    // Pause template (if not already paused)
                    case 'OnPause':
                        switch (this._playState)
                        {
                            case PlayState.Paused:
                                this.debug('OnPause event ignored (already paused)');
                                break;

                            default:
                                this._playState = PlayState.Paused;
                                this.onPause();
                                break;
                        }
                        break;

                    case 'OnVolume':
                        this.onVolume(Number(message.data.volume));
                        break;

                    case 'OnQueueNext':
                        this.onQueueNext(<QueueInfo>message.data);
                        break;

                    case 'OnEditorMessage':
                        this.onEditorMessage(message.data);
                        break;

                    // On schedule attribute set
                    case 'OnAttribute':
                        this.onAttribute(message.data);
                        break;

                    // On geo position coords
                    case 'OnGeoPosition':
                        this.onGeoPosition(message.data);
                        break;

                    case 'OnQueryResponse':
                        this.handleQueryResponse(message.data);
                        break;

                    default:
                        if (!this.minimizeLogNoise)
                            this.debug('Unknown event=' + message.event + ' type="' + message.type + '"');
                        break;
                }
            }, false);

            if (document.readyState == 'complete')
                this.onLoad();
            else
                document.addEventListener('DOMContentLoaded', () => this.onLoad(), false);
        }

        protected viewImplements(possibleFunc: any): possibleFunc is Function {
            return typeof possibleFunc === 'function';
        }

        /**
         * Post a message to the parent window
         */
        private callback(ev: string, data?: any): any {
            if (!this.parentWindow)
                return console.error(`Cannot postMessage() event ${ev}. No parent window found!`);
            if (!this.parentWindow.postMessage)
                throw new Error('postMessage() not available');
            
            return this.parentWindow.postMessage({
                type: 'PubliqEvent',
                event: ev,
                data: data,
                token: this.token,
            }, '*');
        }

        /**
        * Request a screenshot (in thumbs-mode).
        */
        public triggerScreenshot(): void {
            this.callback('OnTriggerScreenshot');
        }

        /**
        * Signal parent window that we're ready to receive shift event
        */
        private preloadComplete(properties: any = null): void {
            this.callback('OnPreloadComplete', properties);
        }

        /**
         * Signal parent window that template has ended
         */
        public templateEnded(message?: (string | Event | OnTemplateEndedReturn)): void {
            let rObj: OnTemplateEndedReturn = {};

            if (message) {
                if (typeof message === 'string')
                    rObj.message = message;
                else if (message instanceof Event) {
                    // Auto generate message if message argument is an event object.
                    // Can be the case if function is assigned as an event handler.
                    try {
                        rObj.message = 'Info: type=' + message.type + ' tagName=' + (<any>message.target).tagName;
                    }
                    catch { }
                }
                else if (typeof message === 'object') {
                    // If an error-object, Clone fails and .message cannot be serialized.
                    rObj = JSON.parse(JSON.stringify(message));
                    rObj.message = message.message;
                }            }
            this.callback('OnTemplateEnded', rObj);
        }

        /**
         * Log a message to the player
         */
        public log(message: string | any, level: LogLevels = LogLevels.debug): void {
            if (typeof message !== 'string') {
                try {
                    message = JSON.stringify(message);
                }
                catch (err) {
                    message = '**MALFORMED MESSAGE**';
                    return;
                }
            }
            this.callback('OnLog', { message: message, level: level });
        }

        public setPersistentItem(key: string, val: any): void {
            try {
                if (val != null) {
                    sessionStorage.setItem(key, JSON.stringify(val));
                    return;
                }

                sessionStorage.removeItem(key);
            }
            catch { }
        }

        public getPersistentItem(key: string, defaultValue?: any): any {
            try {
                let valS = sessionStorage.getItem(key);

                if (typeof valS === 'string')
                    return JSON.parse(valS);
            }
            catch { }

            return defaultValue;
        }

        /**
         * Signal parent window that preload failed.
         *
         * NOTE: Method should only be called during the preload phase (from the onPreload handler).
         *       This will make the player discard its buffer and preload next item.
         */
        private preloadError(message?: (string | Event | Error)): void {
            let rObj: OnPreloadErrorReturn = {};
            if (message) {
                if (typeof message === 'string')
                    rObj.message = message;
                else if (message instanceof Event) {
                    // Auto generate message if message argument is an event object.
                    // Can be the case if function is assigned as an event handler.
                    try {
                        rObj.message = 'Info: type=' + message.type + ' tagName=' + (<any>message.target).tagName;
                    }
                    catch { }
                }
                else if (typeof message === 'object' && (<any>message)?.message) {
                    // If an error-object, Clone fails and .message cannot be serialized.
                    rObj = JSON.parse(JSON.stringify(message));
                    rObj.message = message.message;
                }
            }

            this.callback('OnPreloadError', rObj);
        }

        /**
         * Signal parent window we're ready to accept events
         */
        private cmdReady(): void {
            this.callback('OnCmdReady', {
                apiVersion: this.apiVersion,
                classVersion: this.classVersion,
                format: 'htv'
            } as OnCmdReadyReturn);
        }

        /**
         * Dispatch load event
         */
        private onLoad(): void {
            if (this.viewImplements(this._view.onLoad)) {
                let haveCalledReady = false;
                this._view.onLoad(this, () => {
                    if (!haveCalledReady)
                        this.cmdReady();
                    haveCalledReady = true;
                });
            }
            else
                this.cmdReady();
        }

        /**
         * Dispatch metadata event
         */
        private onMetadata(metadata: Mq.PlayerMetadata): void {
            this.currentMetadata = metadata;
            if (this.viewImplements(this._view.onMetadata)) {
                this._view.onMetadata(metadata);
            }
        }

        protected parseData(data: string): void {
            return JSON.parse(data);
        }

        /**
         * Dispatch OnPreload event
         */
        private onPreload(data: any): void {
            (<Htv.ContentTemplateData>data).__playerMetadata = this.currentMetadata;

            this.currentData = data;

            if (this.viewImplements(this._view.onPreload))
                try {
                    this._view.onPreload(data,
                        (objOrSuggestedPreloadOrRejectPlay?: OnTemplateReadySingleParam, duration?: number, rejectPlay?: boolean) => {

                            let suggestedNextPreload: number = null;
                            let screenshotTime: number;

                            if (typeof objOrSuggestedPreloadOrRejectPlay === 'number')
                                suggestedNextPreload = objOrSuggestedPreloadOrRejectPlay;
                            else if (typeof objOrSuggestedPreloadOrRejectPlay === 'boolean')
                                rejectPlay = objOrSuggestedPreloadOrRejectPlay;
                            else if (objOrSuggestedPreloadOrRejectPlay && typeof objOrSuggestedPreloadOrRejectPlay === 'object') {
                                suggestedNextPreload = objOrSuggestedPreloadOrRejectPlay.suggestedNextPreload;
                                if (typeof duration !== 'number')
                                    duration = objOrSuggestedPreloadOrRejectPlay.duration;
                                if (typeof rejectPlay !== 'boolean')
                                    rejectPlay = objOrSuggestedPreloadOrRejectPlay.rejectPlay;
                                screenshotTime = objOrSuggestedPreloadOrRejectPlay.screenshotTime;
                            }

                            this.suggestedNextPreload = (typeof suggestedNextPreload === 'number') ? suggestedNextPreload : null;
                            duration = (typeof duration === 'number') ? duration : null;
                            rejectPlay = (typeof rejectPlay === 'boolean') ? rejectPlay : null;

                            const returnObj =
                                <Mq.OnPreloadReadyReturn>{
                                    "duration": duration,
                                    "suggestedNextPreload": this.suggestedNextPreload,
                                    "rejectPlay": rejectPlay
                                };
                            if (typeof screenshotTime === 'number')
                                returnObj.screenshotTime = screenshotTime;

                            this.preloadComplete(returnObj);
                        },
                        (msg?: any) => {
                            this.preloadError(msg);
                        }
                    );
                }
                catch (ex) {
                    this.preloadError(ex.message);
                }
        }

        /**
         * Dispatch OnShift event
         */
        private onShift(): void {
            this.onShiftCount++;

            const capturedonShiftCount = this.onShiftCount;
            let onEndCalled = false;

            const onEnded = (msg?: (string | Event | OnTemplateEndedReturn), wasError: boolean = false): void => {
                if (!onEndCalled)
                    if (capturedonShiftCount === this.onShiftCount)
                        this.templateEnded(msg);
                    else
                        this.log(`Late TemplateEnded (intercepted and stopped): "${msg || ''}"`, LogLevels.warning);
                else
                    this.log('TemplateEnded called more than once');

                onEndCalled = true;
            };

            try {
                if (this.viewImplements(this._view.onShift))
                    this._view.onShift(this.onShiftCount, onEnded);
            }
            catch (err) {
                onEnded(err.message, true);
            }
        }

        /**
         * Dipatch OnPlay event
         */
        private onPlay(): void {
            if (this.viewImplements(this._view.onPlay))
                this._view.onPlay();
        }

        /**
         * Dispatch OnPause event
         */
        private onPause(): void {
            if (this.viewImplements(this._view.onPause))
                this._view.onPause();
        }

        /**
         * Dispatch OnVolume
         */
        private onVolume(volume: number): void {
            if (this.viewImplements(this._view.onVolume))
                this._view.onVolume(volume);
        }

        /**
         * Dispatch OnQueueNext
         */
        private onQueueNext(queueInfo: QueueInfo): void {
            if (this.viewImplements(this._view.onQueueNext))
                this._view.onQueueNext(queueInfo);
        }

        /**
         * Dispatch custom editor message
         */
        private onEditorMessage(message: any): void {
            if (this.viewImplements(this._view.onEditorMessage))
                this._view.onEditorMessage(message);
        }

        /**
         * Dispatch changed attribute message
         */
        private onAttribute(attribute: any): void {
            if (this.viewImplements(this._view.onAttribute))
                this._view.onAttribute(attribute as Mq.UpdatedAttribute);
        }

        /**
         * Dispatch geo position coords
         */
        private onGeoPosition(geoCoords: any): void {
            if (this.viewImplements(this._view.onGeoPosition))
                this._view.onGeoPosition(geoCoords as Mq.GeoPosition);
        }

        /**
         * Send custom message to editor
         */
        public editorMessage(message: any): any {
            return this.callback('OnEditorMessage', message);
        }

        /**
         * Set global attribute
         */
        public setAttribute(attribute: Mq.Attribute, force: boolean = false): any {
            (attribute as UpdatedAttribute).force = force;
            return this.callback('OnSetAttribute', attribute);
        }

       /**
        * Send query to template player.
        * Response is handled by handleQueryResponse()
        */
       private queryPlayer(name: string, params: any, callback?: (success: boolean, res: any) => void): void {
           const query = <PlayerQuery>{
               id: Math.round(Math.random() * 1000000000).toString(36), // so we can identify the response
               name: name,
               params: params,
               timeoutHandle: setTimeout(() => {
                   query.timeoutHandle = undefined;
                   if (query.hasBeenHandled)
                       return;
                   query.hasBeenHandled = true;
                   callback?.(false, Error('Query timed out'));
               }, 2000),
               callback: callback
           };
           this._queries[query.id] = query;
           this.callback('OnQuery', { id: query.id, name: name, params: params });
        }

        /**
         * Handle queryPlayer() response from MqTemplatePlayer
         */
        private handleQueryResponse(res: any): void {

            const query = this._queries[res.id];
            if (query) {
                this._queries[res.id] = undefined;

            if (query.timeoutHandle != undefined)
                clearTimeout(query.timeoutHandle);

            if (!query.hasBeenHandled) {
                query.hasBeenHandled = true;
                query.callback?.(res.result, res.response);
            }
        }
        else
            this.log(`Unknown query response id=${res.id}`);
    }

    /**
     * Get persistent item that is scoped to the current channel.
     */
    public getPersistentChannelItem<T>(key: string, callback: (res: T) => void, defaultValue?: T): void {
        this.queryPlayer('getPersistentItem', { key: key },
            (success, res) => {
                if (success)
                    callback(res);
                else
                    callback(defaultValue);
            });
    }

    /**
     * Set persistent item that is scoped to the current channel.
     * The optional callback is called when item has been set.
     */
    public setPersistentChannelItem(key: string, value: any, callback?: (res: { result: boolean; error?: Error }) => void): void {
        this.queryPlayer('setPersistentItem', { key: key, value: value },
            (success, res) => {
                if (typeof callback === 'function')
                    if (success)
                        callback({ result: true });
                    else
                        callback({ result: true, error: res });
            });
        }

        /**
         * Internal debugging
         */
        private debug(str: string): void {
            console.log(str);
        }

        /**
         * Set global proof-of-play extra data.
         * Per item extra data can be set via preloadComplete()/templateEnded()
         */
        public setGlobalProofExtraData(data: any): any {
            return this.callback('OnSetGlobalProofExtraData', data);
        }

        /**
         * Log a Proof of play event.
         */
        public logProofOfPlay(params: ProofOfPlayLogEntry): void {
            (<any>params).mediaId = params.id;
            delete params.id;

            return this.queryPlayer('logProofOfPlay', params);
        }
    }
}
