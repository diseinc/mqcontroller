
namespace Mq.Media {

    /* With the "normal" ordering - some templates may differ.
     * Note that the actual names will be used as CSS-class specifiers,
     * so beware of changing them... You may break the world.
     */
    export enum AspectMode {
        undefined = -1,
        fitWidth = 0,
        fitHeight,
        letterbox,
        crop,
        stretch
    }

    export type MediaHandlerType = ('video' | 'picture');

    export interface IMediaHandler {
        onEnded: () => void;
        preload(url: string): Promise<number>;

        fallbackDuration: number;
        mediaDuration: number;
        silent: boolean;
        stillframeTime: number;

        type: MediaHandlerType;
        start(): Promise<void>;
        // ZZZ: Document stages.
        getStartStages(): (() => void)[];
        remove(): void;

        setVolume(volume: number): void;
    }

    export interface IVideoHandler extends IMediaHandler {
        type: 'video';
        videoElement: HTMLVideoElement;
    }
    export interface IPictureHandler extends IMediaHandler {
        type: 'picture';
        imgElement: HTMLImageElement;
    }

    export type MediaHandler = IVideoHandler | IPictureHandler;

    export abstract class BaseMediaHandler {
        protected container: HTMLElement;
        private isEnded: boolean = false;
        protected isRemoved = false;
        public abstract type: MediaHandlerType;

        public abstract fallbackDuration: number;
        public abstract mediaDuration: number;
        public abstract silent: boolean;
        public abstract stillframeTime: number;

        static priority: number;
        static canPlayContent(url: string, type?: string) {
            return false;
        }

        /**
         * Callback for media ended or timeout
         */
        onEnded: () => void = null;

        constructor(protected fadeInTime: number, protected fadeOutTime?: number) {
        }

        protected callEnd(): void {
            if (!this.isEnded && !this.isRemoved) {
                this.isEnded = true;

                this.onEnded?.();
            }
        }

        protected activateFadeIn(): void {
            if (this.fadeInTime) {
                this.container.style.animationDuration = this.fadeInTime + 's';
                this.container.classList.add('intro')
            }
        }

        protected activateFadeOut(): void {
            if (this.fadeOutTime) {
                this.container.style.animationDuration = this.fadeOutTime + 's';
                this.container.classList.add('outro')
            }
        }

        protected remove(): void {
            if (!this.isRemoved) {
                this.isRemoved = true;
                if (this.container?.parentElement)
                    this.container.parentElement.removeChild(this.container);
                this.container = null;
            }
        }

        static aspectModeFromString(name: string): number {
            name = name.toUpperCase();

            for (let v in AspectMode)
                if (v.toUpperCase() === name)
                    return Number(AspectMode[v]);

            return AspectMode.undefined;
        }

        static supportedExtensions = [];

        static matchesExtension(name: string): boolean {
            name = /(.*)(\?|$)+/.exec(name)[1];
            const lastDot = name.lastIndexOf('.');

            if (lastDot < 0 || this.supportedExtensions.length === 0)
                return false;

            const ext = name.substr(lastDot + 1).toLowerCase();

            return this.supportedExtensions.some((v) => v == ext);
        }
    }
}
