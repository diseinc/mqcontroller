
namespace Mq.Media.Data {
    
    export const enum MediaType {
        Video = 'Video',
        Picture = 'Picture',
        Audio = 'Audio'
    }

    export interface MediaPreloadInfo {
        mediaType?: MediaType;
        url: string;
        mimeType?: string;
        duration?: number;
        mediaDuration?: number;
        width?: number;
        height?: number;
        aspect?: number;
        volume?: number;

        aspectMode?: AspectMode;

        fadeInDuration?: number;
        fadeOutDuration?: number;

        hideUntilStart?: boolean;

        muted?: boolean;

        stillFrameTime?: number;

        videoScale?: [xScale: number, yScale: number];
    }

    export function getMediaPreloadInfo(media: Htv.ContentFile,
        data: Htv.ContentTemplateData,
        duration: number,
        isFirstMediaPage: boolean,
        defaultAspectMode: AspectMode = AspectMode.letterbox): MediaPreloadInfo {

        const helper = Htv.DataHelper.getNonCached(data);

        const aspectMode = helper.content.enums['AspectMode'] ??
            (helper.template.settings['AspectMode']
                ? BaseMediaHandler.aspectModeFromString(data.settings['AspectMode'])
                : defaultAspectMode);

        const scaleExp = /^\s*([\.0-9]+)\s+([\.0-9]+)/;
        const videoScaleSetting = helper.template.settings['VideoScale'];
        let videoScale: [xScale: number, yScale: number];

        if (videoScaleSetting && scaleExp.test(videoScaleSetting)) {
            const [, xs, ys] = scaleExp.exec(videoScaleSetting);
            videoScale = [Number(xs), Number(ys)];
        }

        let tmp: number;

        return {
            url: media.url,
            mediaType: media.properties['type'],
            muted: !!helper.template.settings['Silent'] || !!helper.content.enums['Silent'],
            fadeInDuration: Number(helper.template.settings['FadeIn'] ?? 0),
            fadeOutDuration: Number(helper.template.settings['FadeOut'] ?? 0),
            aspectMode: aspectMode,
            duration: duration,
            mediaDuration: (tmp = Number(media.properties['duration'] ?? 0)) != 0 ? tmp : undefined,
            volume: helper.player.volume,
            hideUntilStart: !isFirstMediaPage,
            aspect: getMediaAspect(media),
            width: (tmp = Number(media.properties['width'] ?? 0)) != 0 ? tmp : undefined,
            height: (tmp = Number(media.properties['height'] ?? 0)) != 0 ? tmp : undefined,
            stillFrameTime: !helper.player.metadata.thumbnailRecordingMode
                ? undefined
                : ((tmp = helper.template.settings['ScreenshotTime']) != null
                    ? Number(tmp) : 1.5),
            videoScale: videoScale
        };
    }

    export function getMediaFiles(data: Htv.ContentTemplateData, defaultMediaFieldName: string, append?: string): Htv.ContentFile[] {

        let mediaField = data.data[0][getMediaFieldName(data, defaultMediaFieldName, true, append)];

        if (!Htv.DataHelper.isMediaField(mediaField) || mediaField.files.length == 0)
            mediaField = data.data[0][getMediaFieldName(data, defaultMediaFieldName, false, append)];

        if (mediaField && Htv.DataHelper.isMediaField(mediaField))
            return (mediaField as Htv.FilesField).files;

        return [];
    }

    function getPreOrSuffixes(
        templateSettings: Mq.StringMap<any>,
        attributes: Mq.StringMap<Mq.Attribute>,
        singleValSetting: string,
        multiValSetting: string,
        multiValIndex: number,
        attributeLookupSetting: string): string {

        if (multiValSetting) {
            const val = templateSettings[multiValSetting];

            if (val)
                if (typeof val === 'string')
                    return String(val);
                else if (Array.isArray(val) && (<any[]>val).length >= 1)
                    return String(val[multiValIndex % val.length]);
        }

        const val: any = templateSettings[singleValSetting];

        if (typeof val === 'string')
            return String(val);

        // Fallback to attribute-setting, typically from a static-attr in the channel.
        if (attributeLookupSetting && attributes) {
            const val = templateSettings[attributeLookupSetting];

            if (typeof val === 'string') {
                const attr = attributes[val];

                if (attr && attr.data)
                    return attr.data;
            }
        }

        return '';
    }

    export function getMediaFieldName(
        data: Htv.ContentTemplateData,
        defaultFieldName: string,
        includePreSuffixes: boolean = true,
        append: string = ''): string {

        const helper = Htv.DataHelper.getNonCached(data);
        const templateSettings = helper.template.settings;

        const baseName = (templateSettings['MediaField'] || defaultFieldName) + append;

        if (!includePreSuffixes)
            return baseName;

        const attr = helper.player.attributes['dyn.FieldFixIndex'] as Mq.Attribute;

        const fixIndex = (attr && attr.data) ? Number(attr.data) : 0;

        const prefix = getPreOrSuffixes(templateSettings,
            <Mq.StringMap<Mq.Attribute>>helper.player.attributes, "FieldPrefix", "FieldPrefixes", fixIndex, "FieldPrefixAttribute");
        const suffix = getPreOrSuffixes(templateSettings,
            <Mq.StringMap<Mq.Attribute>>helper.player.attributes, "FieldSuffix", "FieldSuffixes", fixIndex, "FieldSuffixAttribute");

        return prefix + baseName + suffix;
    }

    export function getMediaAspect(media: Htv.ContentFile): number {
        const w: number = media?.properties?.['width'];
        const h: number = media?.properties?.['height'];
        let res: number = 0;

        if (w && h) {
            const pAspect = media?.properties?.['pixelAspect'] || 1;
            res = w * pAspect / h;
        }

        return res;
    }
}
