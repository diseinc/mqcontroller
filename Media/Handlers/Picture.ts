
namespace Mq.Media.Handlers {

    /**
     * Handles picture preload and animation sequence.
     */
    export class Picture extends BaseMediaHandler implements IPictureHandler {
        type: 'picture' = 'picture';
        public imgElement: HTMLImageElement;
        private duration: number = 0;
        public fallbackDuration: number = 0;
        public mediaDuration: number;
        public silent: boolean;
        public stillframeTime: number;

        static mediaType: Data.MediaType = Data.MediaType.Picture;
        static priority: number = 100;
        static supportedExtensions = [
            'jpg', 'png', 'jpeg',
            'gif', 'bmp', 'tif',
            'tiff', 'svg', 'webp', 'avif',
        ];
        static supportedMimeTypes = [
            'image/jpeg',
            'image/png', 'image/apng', 'image/x-png',
            'image/gif', 'image/bmp',
            'image/tiff', 'image/svg+xml',
            'image/webp', 'image/avif',
        ];

        static canPlayContent(url: string, type?: string) {
            const lcType = type?.toLowerCase()
            return type === this.mediaType ||
                (lcType && this.supportedMimeTypes.some(mt => mt == lcType)) ||
                (!type && this.matchesExtension(url));
        }

        constructor(private connectAsHiddenContainer: (element: HTMLElement, handlerType: MediaHandlerType) => HTMLElement,
            fadeInTime?: number, fadeOutTime?: number) {

            super(fadeInTime, fadeOutTime);

            this.imgElement = document.createElement('img');
            this.imgElement.classList.add('picture');
        }

        /**
         * Preload picture and setup event listeners
         */
        preload(url: string): Promise<number> {
            this.container = this.connectAsHiddenContainer(this.imgElement, this.type);
            this.duration = this.fallbackDuration;

            // Promise resolves when preload is complete
            let promise = new Promise<number>((resolve, reject) => {

                this.container.addEventListener('animationend',
                    (ev: AnimationEvent) => {
                        if (ev.animationName === 'outro')
                            this.callEnd();
                    });

                // Callback when picture is ready
                this.imgElement.onload = () => resolve(this.duration);

                // Reject on error
                this.imgElement.onerror = (e: Event) => {
                    reject(Error('Picture error handler fired'));
                    this.imgElement.onerror = null;
                };

                this.imgElement.src = url;
            });

            return promise;
        }

        start(): Promise<void> {

            this.container.style.visibility = "inherit";
            this.activateFadeIn();

            setTimeout(() => this.callEnd(), this.duration * 1000);

            // Handle outro - delay it slightly to avoid flashing in certain situations
            if (this.fadeOutTime && this.fadeOutTime > 0)
                setTimeout(() => this.activateFadeOut(),
                    (this.duration - this.fadeOutTime + (2 / 60)) * 1000);

            return Promise.resolve();
        }

        getStartStages(): (() => void)[] {
            return [
                null,
                () => this.start()
            ];
        }

        setVolume(volume: number): void { }

        remove(): void {
            if (this.isRemoved)
                return;

            this.imgElement.onerror = null;
            this.imgElement.onload = null;

            super.remove();
            this.imgElement = null;
        }
    }
}
