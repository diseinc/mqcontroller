
namespace Mq.Media.Handlers {

    /**
     * Handles video preload and animation sequence.
     */
    export class Video extends BaseMediaHandler implements IVideoHandler {
        type: 'video' = 'video';
        public videoElement: HTMLVideoElement;
        private duration: number;
        public fallbackDuration: number = 0;
        public mediaDuration: number;
        public silent: boolean;
        private volume: number;
        public stillframeTime: number;
        protected isStarted: boolean = false;

        static mediaType: Data.MediaType = Data.MediaType.Video;
        static priority: number = 10;

        static supportedExtensions = [
            'mp4', 'webm', 'wmv',
            'mpg', 'm2p', 'm2v',
            'mov', 'm4v', 'mpg', 'mpeg',
            'avi',
            'ogg' // <-- ???: Also pure audio?
        ];
        static supportedMimeTypes = [
            'video/mp4', 'video/webm',
            'video/mpeg', 'video/mp2p',
            'video/quicktime',
            'video/ms-wmv', 'video/x-ms-wmv',
            'video/msvideo', 'video/x-msvideo',
            'video/ogg'
        ];

        static canPlayContent(url: string, type?: string) {
            const lcType = type?.toLowerCase()
            return type === this.mediaType ||
                (lcType && this.supportedMimeTypes.some(mt => mt == lcType)) ||
                (!type && this.matchesExtension(url));
        }

        constructor(private connectAsHiddenContainer: (element: HTMLElement, handlerType: MediaHandlerType) => HTMLElement,
            fadeInTime?: number, fadeOutTime?: number) {

            super(fadeInTime, fadeOutTime);

            this.videoElement = document.createElement('video');
            this.videoElement.muted = true;
            this.videoElement.classList.add('video');
        }

        /**
         * Preload video and setup event listeners
         */
        preload(url: string): Promise<number> {
            this.container = this.connectAsHiddenContainer(this.videoElement, this.type);

            if (this.silent)
                this.setVolume(0);

            // Promise resolves when preload is complete
            return new Promise<number>((resolve, reject) => {
                this.videoElement.src = url;
                this.videoElement.controls = false;
                this.videoElement.preload = 'auto';

                // Listen to animationend events
                this.container.addEventListener('animationend',
                    (ev: AnimationEvent) => {
                        if (ev.animationName === 'outro')
                            this.callEnd();
                    });

                // Reject on error
                this.videoElement.onerror = (e: Event) => {
                    this.videoElement.onerror = null;

                    console.log('Video error: reject: ' + e.type);
                    console.log("Error " + this.videoElement.error.code + "; details: " + ((<any>this.videoElement).error?.message ?? ""));

                    reject(Error('Video error handler fired'));
                };

                let haveAddedOutro: boolean = !this.fadeOutTime;

                const onTimeUpdate = () => {
                    const v = this.videoElement;

                    try {
                        if (!v || isNaN(v.duration) || this.duration == null)
                            return;

                        // Check if we should add the outro class
                        let remains = this.duration - v.currentTime;

                        if (!isNaN(remains) && remains <= 0) {
                            this.videoElement.ontimeupdate = null;
                            this.videoElement?.removeEventListener('timeupdate', onTimeUpdate);
                            this.callEnd();
                        }

                        if (!haveAddedOutro && !isNaN(remains) && remains < this.fadeOutTime) {
                            this.activateFadeOut();
                            haveAddedOutro = true;
                        }
                    }
                    catch (e) {
                        // ??? We'd better close up shop.
                        this.videoElement?.removeEventListener('timeupdate', onTimeUpdate);
                        this.callEnd();
                    }
                };

                const onCanPlayThrough = () => {
                    this.videoElement.removeEventListener('canplaythrough', onCanPlayThrough);
                    this.videoElement.onerror = null;

                    this.duration = (this.videoElement?.duration ?? 0) > 0
                        ? this.videoElement.duration
                        : (this.mediaDuration || this.fallbackDuration);

                    // Handle outro
                    this.videoElement.addEventListener('timeupdate', onTimeUpdate);

                    // Workaround for Android:
                    this.videoElement.play();
                    this.videoElement.pause();

                    // Loaded ok - resolve
                    resolve(this.duration);
                };

                // Callback when video is ready
                this.videoElement.addEventListener('canplaythrough', onCanPlayThrough);

                // Video ended
                this.videoElement.onended = () => {
                    this.callEnd();
                };

                // Start video preloading
                this.videoElement.load();
            }).then((duration) => {
                if (typeof this.stillframeTime !== 'number' || !this.videoElement.seekable)
                    return duration;

                this.videoElement.pause();
                let snapPos = this.stillframeTime;
                if (snapPos >= duration)
                    snapPos = duration * 0.1;

                return new Promise((resolve, reject) => {
                    this.videoElement.onerror = (e: Event) => {
                        this.videoElement.onerror = null;

                        reject(Error('Video error handler fired'));
                    };

                    this.videoElement.onseeked = (ev) => {
                        this.videoElement.onseeked =
                        this.videoElement.onerror = null;

                        resolve(duration);
                    };
                    this.videoElement.currentTime = snapPos;
                });
            });
        }

        start(): Promise<void> {
            this.isStarted = true;
            if (!this.silent)
                this.setVolume(this.volume ?? 1);

            this.activateFadeIn();
            this.container.style.visibility = "inherit";
            if (this.stillframeTime == null) {
                if (!isIE())
                    return this.videoElement.play()
                        .catch(() => {
                            this.videoElement.muted = true;
                            const clickHandler = () => {
                                this.setVolume(this.volume ?? 1);
                                this.videoElement.removeEventListener('click', clickHandler);
                            };
                            this.videoElement.addEventListener('click', clickHandler);
                            return this.videoElement.play();
                        });

                this.videoElement.play();
            }
            else
                setTimeout(() => this.callEnd(), this.duration * 1000);

            return Promise.resolve();
        }

        getStartStages(): (() => void)[] {
            return [
                () => this.videoElement.play(),
                () => {
                    this.activateFadeIn();
                    this.container.style.visibility = "inherit";
                }
            ];
        }

        setVolume(volume: number): void {
            this.volume = volume;

            if (!this.isStarted)
                return;

            if (this.videoElement && volume != null) {
                const paused = this.videoElement.paused;

                this.videoElement.volume = this.silent ? 0 : volume;
                this.videoElement.muted = this.silent || volume === 0;

                // Modern standard browsers will pause the video if
                // the user haven't interacted with the document:
                if (!paused && this.videoElement.paused) {
                    this.videoElement.muted = true;
                    this.videoElement.play();
                }
            }
        }

        /**
         * Unload video
         */
        remove(): void {
            if (this.isRemoved)
                // Second call to this function - ignore
                return;

            // Unregister event handlers
            this.videoElement.onerror =
            this.videoElement.oncanplay =
            this.videoElement.oncanplaythrough =
            this.videoElement.onended =
            this.videoElement.ontimeupdate =
            this.videoElement.onstalled = null;

            this.videoElement.volume = 0; // mute

            // this.video.src = 'data:video/mp4;base64,===';
            this.videoElement.pause();
            this.videoElement.src = ''; // flickering?
            this.videoElement.load();

            if (this.videoElement.parentElement)
                this.videoElement.parentElement.removeChild(this.videoElement);
            super.remove();
            this.videoElement = null;
        }
    }
}
