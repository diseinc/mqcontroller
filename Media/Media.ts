/// <reference path="../HtvModel.d.ts" />
/// <reference path="../DataHelper.ts" />
/// <reference path="../Htv.DataHelper.ts" />
/// <reference path="./MediaHandlers.ts" />
/// <reference path="./Media.Data.ts" />
/// <reference path="./Handlers/Picture.ts" />
/// <reference path="./Handlers/Video.ts" />

namespace Mq.Media {

    // ZZZ: Move to Mq.Utility or something:
    export function isTizen(): boolean {
        return !!navigator.userAgent.match(/Tizen/i);
    }
    export function isAndroid(): boolean {
        return !!navigator.userAgent.match(/Android/i);
    }
    export function isIE(): boolean {
        return /MSIE |Trident/.test(navigator.userAgent);
    }

    function getMediaContainer(container: HTMLElement, createAsHidden: boolean = true): HTMLElement {
        let div = document.createElement('div');
        if (createAsHidden)
            div.style.visibility = "hidden";
        container.appendChild(div);
        return div;
    }

    export function addMediaForPreload(container: HTMLElement,
        media: Htv.ContentFile,
        data: Htv.ContentTemplateData,
        duration: number,
        isFirstMediaPage: boolean,
        defaultAspectMode?: AspectMode): MediaHandler;
    export function addMediaForPreload(container: HTMLElement,
        mediaInfo: Data.MediaPreloadInfo,
        duration?: number): MediaHandler;
    export function addMediaForPreload(container: HTMLElement,
        mediaOrMediaInfo: Htv.ContentFile | Data.MediaPreloadInfo,
        dataOrDuration?: Htv.ContentTemplateData | number,
        duration?: number,
        isFirstMediaPage?: boolean,
        defaultAspectMode: AspectMode = AspectMode.letterbox): MediaHandler {

        if (dataOrDuration &&
            typeof dataOrDuration === 'object' &&
            !isPreloadMediaInfo(mediaOrMediaInfo)) {

            return addMediaForPreload(container, Data.getMediaPreloadInfo(
                <Htv.ContentFile>mediaOrMediaInfo, <Htv.ContentTemplateData>dataOrDuration, duration, isFirstMediaPage, defaultAspectMode
            ));
        }

        const mi: Data.MediaPreloadInfo = mediaOrMediaInfo;

        if (typeof dataOrDuration === 'number')
            duration = dataOrDuration;
        else
            duration = mi.duration;

        let handler: MediaHandler = null;

        if (!mi.aspect && mi.width && mi.height)
            mi.aspect = mi.width / mi.height;

        const handlers = Object.keys(Handlers);
        handlers.sort((v1, v2) => Handlers[v2].priority - Handlers[v1].priority);

        for (const handlerName of handlers)
            if (Handlers[handlerName].canPlayContent(mi.url, mi.mimeType || mi.mediaType)) {
                handler = new Handlers[handlerName]((element, handlerType: MediaHandlerType) => {
                    if (handlerType === 'video')
                        return addVideo(getMediaContainer(container, mi.hideUntilStart ?? true),
                            element, mi.aspectMode ?? AspectMode.letterbox, mi.aspect, mi.videoScale);
                    return addPicture(getMediaContainer(container, mi.hideUntilStart ?? true),
                        element, mi.aspectMode ?? AspectMode.letterbox, mi.aspect, mi.url);
                }, mi.fadeInDuration, mi.fadeOutDuration);
                
                handler.fallbackDuration = duration;
                handler.mediaDuration = mi.mediaDuration;
                handler.silent = mi.muted ?? false;
                handler.stillframeTime = mi.stillFrameTime;
                handler.setVolume(mi.volume);
                break;
            }

        if (!handler)
            throw new TypeError("Unsupported media-type " + mi.mediaType);

        return handler;
    }

    export function preloadMedia(container: HTMLElement, media: Htv.ContentFile,
        data: Htv.ContentTemplateData,
        duration: number,
        isFirstMediaPage: boolean,
        defaultAspectMode?: AspectMode): Promise<[MediaHandler, number]>;
    export function preloadMedia(container: HTMLElement,
        mediaInfo: Data.MediaPreloadInfo,
        duration?: number): Promise<[MediaHandler, number]>;
    export function preloadMedia(container: HTMLElement,
        mediaOrMediaInfo: Htv.ContentFile | Data.MediaPreloadInfo,
        dataOrDuration?: Htv.ContentTemplateData | number,
        duration?: number,
        isFirstMediaPage?: boolean,
        defaultAspectMode?: AspectMode): Promise<[MediaHandler, number]> {

        let mi: Data.MediaPreloadInfo;

        if (!isPreloadMediaInfo(mediaOrMediaInfo))
            mi = Data.getMediaPreloadInfo(
                mediaOrMediaInfo, <Htv.ContentTemplateData>dataOrDuration, duration, isFirstMediaPage, defaultAspectMode);
        else {
            mi = mediaOrMediaInfo;
            duration = typeof dataOrDuration === 'number'
                ? dataOrDuration
                : mi.duration;
        }

        const mediaHandler: MediaHandler = addMediaForPreload(container, mi, duration);

        return mediaHandler.preload(mi.url)
            .then((duration) => [mediaHandler, duration]);
    }

    function isPreloadMediaInfo(data: any): data is Data.MediaPreloadInfo {

        return data && typeof data === 'object' &&
            typeof data.url === 'string' &&
            data.name !== null &&
            typeof data.name !== 'string' &&
            typeof data.properties !== 'object';
    }

    function addVideo(container: HTMLElement,
        video: HTMLElement | HTMLVideoElement,
        aspectMode: Mq.Media.AspectMode, mediaAspect: number,
        videoScale?: [xScale: number, yScale: number]): HTMLElement {

        container.appendChild(video);
        video.classList.add(Mq.Media.AspectMode[aspectMode]);

        inheritCSSClasses(video, container);

        container.classList.add("mediaContainer");

        const setScale = (aspect: number): void => {
            const containerAspect = container.clientWidth / container.clientHeight;
            if (Math.abs(containerAspect - aspect) > 0.001 || videoScale) {

                const wider: boolean = aspect > containerAspect;
                const scale = wider ? aspect / containerAspect : containerAspect / aspect;

                const scaleX = (aspectMode === Mq.Media.AspectMode.crop) ||
                    (wider && aspectMode === Mq.Media.AspectMode.fitHeight) ||
                    (!wider && ((aspectMode === Mq.Media.AspectMode.fitWidth) || (aspectMode === Mq.Media.AspectMode.stretch)));
                const scaleY = (aspectMode === Mq.Media.AspectMode.crop) ||
                    (!wider && aspectMode === Mq.Media.AspectMode.fitWidth) ||
                    (wider && ((aspectMode === Mq.Media.AspectMode.fitHeight) || (aspectMode === Mq.Media.AspectMode.stretch)));
                let transformScaleX = scaleX ? scale : 1;
                let transformScaleY = scaleY ? scale : 1;

                if (videoScale) {
                    transformScaleX *= videoScale[0];
                    transformScaleY *= videoScale[1];
                }

                //// Alternative:
                // let transformScaleX = 1, transformScaleY = 1;

                // if (wider)
                //    switch (aspectMode) {
                //        case AspectMode.crop:
                //        case AspectMode.fitHeight:
                //            transformScaleX = transformScaleY = scale;
                //            break;
                //        case AspectMode.stretch:
                //            transformScaleY = scale;
                //            break;
                //    }
                // else
                //    switch (aspectMode) {
                //        case AspectMode.crop:
                //        case AspectMode.fitWidth:
                //            transformScaleX = transformScaleY = scale;
                //            break;
                //        case AspectMode.stretch:
                //            transformScaleX = scale;
                //            break;
                //    }

                if (transformScaleX != 1 || transformScaleY != 1)
                    video.style.transform = `scale(${transformScaleX},${transformScaleY})`;
            }
        };

        if (aspectMode != Mq.Media.AspectMode.letterbox || videoScale)
            if (mediaAspect)
                setScale(mediaAspect);
            else {
                const onCanPlayThrough = () => {
                    video.removeEventListener('canplaythrough', onCanPlayThrough);

                    const w = (<HTMLVideoElement>video).videoWidth;
                    const h = (<HTMLVideoElement>video).videoHeight;

                    if (w && h)
                        setScale(w / h);
                };

                // Callback when video is ready
                video.addEventListener('canplaythrough', onCanPlayThrough);
            }

        return container;
    }

    function addPicture(container: HTMLElement,
        picture: HTMLElement | HTMLImageElement,
        aspectMode: Mq.Media.AspectMode, mediaAspect: number, url: string): HTMLElement {

        container.appendChild(picture);
        picture.classList.add(Mq.Media.AspectMode[aspectMode]);

        switch (aspectMode) {
            case Mq.Media.AspectMode.crop:
            case Mq.Media.AspectMode.letterbox:
                picture.classList.add("doubleAxis");
                break;
            default:
                picture.classList.add("singleAxis");
                break;
        }

        inheritCSSClasses(picture, container);

        container.classList.add("mediaContainer");
        return container;
    }

    function inheritCSSClasses(source: HTMLElement, target: HTMLElement): void {
        for(let i = 0; i <source.classList.length; i++)
            target.classList.add(source.classList[i]);
    }
}
