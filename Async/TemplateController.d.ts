/// <reference path="../MqModel.d.ts" />

declare namespace Mq.Async {

    interface ITemplateController {
        readonly apiVersion: number;
        readonly classVersion: number;

        templateEnded(message?: (string | Event | OnTemplateEndedReturn)): void;
        templateError(message: (string | Event | Error)): void;
        setAttribute(attribute: Mq.Attribute, force?: boolean): any;

        editorMessage(message: any): any;
        log(message: string | any, level?: Mq.LogLevels): void;

        setGlobalProofExtraData(data: any): any;
        logProofOfPlay(params: ProofOfPlayLogEntry): void;

        setPersistentItem(key: string, val: any): void;
        getPersistentItem(key: string, defaultValue?: any): any;

        getPersistentChannelItem<T>(key: string, defaultValue?: T): Promise<T>;
        setPersistentChannelItem(key: string, value: any): Promise<void>;

        setLayerProperties(labelOrId: string, properties: Partial<Mq.LayerProperties>): Promise<void>;
        getLayerProperties(labelOrId: string): Promise<Mq.LayerProperties>;

        triggerScreenshot(): void;

        delay(ms: number, rejectOnEnd?: boolean): Promise<void>;

        getConfig<T extends Mq.TemplateConfig>(checkData?: (data: any) => data is T): Promise<T>;
        getConfig<T extends Mq.TemplateConfig>(options?: {
            verifyData?: (data: any) => data is T;
            url?: string}): Promise<T>;
    }
}
