/// <reference path="./TemplateView.d.ts" />
/// <reference path="./TemplateController.d.ts" />
/// <reference path="../DataHelper.ts" />

namespace Mq.Async {

    export class TemplateViewBase implements TemplateView {
        protected mqController: Mq.Async.ITemplateController;

        async onLoad(controller?: Mq.Async.ITemplateController): Promise<Mq.OnLoadCompleteReturn> {
            this.mqController = controller;
        }
    }
}
