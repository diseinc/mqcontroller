/// <reference path="./TemplateController.d.ts" />
/// <reference path="./TemplateView.d.ts" />

namespace Mq.Async {

    const enum PlayState {
        Playing,
        Paused
    }

    interface PlayerQuery {
        id: string;
        name: string;
        params: any;
        callback?: (success: boolean, res: any) => void;
    }

    /**
     * Template controller
     */
    export class TemplateController implements ITemplateController {
        private _view: TemplateView = null;

        private currentMetadata: Mq.PlayerMetadata = null;
        currentData: any = null;

        private _playState: PlayState = PlayState.Playing;

        private _queries: Mq.StringMap<PlayerQuery> = {};
        private onShiftCount: number = 0;
        private token: string;
        private parentWindow: Window;

        jsPlayerCaps: number = 0;

        // Version of the jsPlayer <> MqTemplate API
        public readonly apiVersion: number = 9;
        // MqTemplate version
        // Version history:
        //    3 : 2015-??-??
        //    4 : 2015-12-17 - added onQueueNext, added metadata.groupRelativePath and metadata.lastQueueNumber
        //    5 : 2016-11-08 - added onEditorMessage, editorMessage
        //    6 : 2017-06-29 - metadata.attributes, metadata.jsPlayerCapabilities, onAttribute, setAttribute, onGeoPosition,
        //                     preloadComplete params duration, rejectPlay and suggestedNextPreload
        //    7 : 2017-06-30 - query api: setPersistentItem, getPersistentItem   
        //    8 : 2019-03-06 - added setGlobalProofExtraData
        //    9 : 2020-08-14 - added logProofOfPlay
        public readonly classVersion: number = 9;

        constructor(view?: TemplateView, private minimizeLogNoise: boolean = false) {

            this._view = view ? view : <TemplateView>{};

            this.parentWindow = window !== window.parent ? window.parent : null;
            // Get event token from get param token=
            this.token = window.location.search.match(/[?&]token=([^&#]+)/)?.[1] || 'default-token';

            // Listen to messages posted by parent window
            window.addEventListener('message', (ev: MessageEvent) => {
                const message = ev.data;

                if (!message || typeof message !== 'object') {
                    return;
                }

                // Should be a jsPlayerEvent
                if (message.type !== 'jsPlayerEvent') {
                    if (!this.minimizeLogNoise)
                        this.debug('Event ignored:  event=' + message.event + ' type="' + message.type + '"');
                    return;
                }
                //this.debug('Got event=' + message.event + ' type=' + message.type);

                // Handle events
                switch (message.event) {
                    // Metadata from player
                    case 'OnMetadata':
                        this.onMetadata(message.data);
                        break;

                    // Preload data
                    case 'OnPreload':
                        let parsedData: any = null;
                        try {
                            // Call the onParse method. Default action is JSON.parse
                            parsedData = this.parseData(message.data);
                        }
                        catch (err) {
                            // Failed to parse - can't continue
                            this.templateError('onParse failed: ' + err.message + ' data: ' + message.data);
                            break; // Note: early break
                        }
                        this.onPreload(parsedData);
                        break;

                    // Show/shift template
                    case 'OnShift':
                        // Default playState after OnShift event is playing
                        this._playState = PlayState.Playing;
                        this.onShift();
                        break;

                    // Play template (if not already playing)
                    case 'OnPlay':
                        switch (this._playState)
                        {
                            case PlayState.Playing:
                                this.debug('OnPlay event ignored (already playing)');
                                break;

                            default:
                                this._playState = PlayState.Playing;
                                this.onPlay();
                                break;
                        }
                        break;

                    // Pause template (if not already paused)
                    case 'OnPause':
                        switch (this._playState)
                        {
                            case PlayState.Paused:
                                this.debug('OnPause event ignored (already paused)');
                                break;

                            default:
                                this._playState = PlayState.Paused;
                                this.onPause();
                                break;
                        }
                        break;

                    case 'OnVolume':
                        this.onVolume(Number(message.data.volume));
                        break;

                    case 'OnQueueNext':
                        this.onQueueNext(<QueueInfo>message.data);
                        break;

                    case 'OnEditorMessage':
                        this.onEditorMessage(message.data);
                        break;

                    // On schedule attribute set
                    case 'OnAttribute':
                        this.onAttribute(message.data);
                        break;

                    // On geo position coords
                    case 'OnGeoPosition':
                        this.onGeoPosition(message.data);
                        break;

                    case 'OnQueryResponse':
                        this.handleQueryResponse(message.data);
                        break;

                    default:
                        if (!this.minimizeLogNoise)
                            this.debug('Unknown event=' + message.event + ' type="' + message.type + '"');
                        break;
                }
            }, false);

            if (document.readyState == 'complete')
                this.onLoad();
            else
                document.addEventListener('DOMContentLoaded', () => this.onLoad(), false);
        }

        protected viewImplements(possibleFunc: any): possibleFunc is Function {
            return typeof possibleFunc === 'function';
        }

        /**
         * Post a message to the parent window
         */
        private callback(ev: string, data?: any): any {
            if (!this.parentWindow)
                return console.error(`Cannot postMessage() event ${ev}. No parent window found!`);
            if (!this.parentWindow.postMessage)
                throw Error('postMessage() not available');

            return this.parentWindow.postMessage({
                type: 'PubliqEvent',
                event: ev,
                data: data,
                token: this.token,
            }, '*');
        }

        /**
        * Request a screenshot (in thumbs-mode).
        */
        public triggerScreenshot(): void {
            this.callback('OnTriggerScreenshot');
        }

        /**
        * Signal parent window that we're ready to receive shift event
        */
        private preloadComplete(properties: any = null): void {

            this.callback('OnPreloadComplete', properties);
        }

        /**
         * Signal parent window that template has ended
         */
        public templateEnded(message?: (string | Event | OnTemplateEndedReturn)): void {
            let rObj: OnTemplateEndedReturn = {};

            if (message) {
                if (typeof message === 'string')
                    rObj.message = message;
                else if (message instanceof Event) {
                    // Auto generate message if message argument is an event object.
                    // Can be the case if function is assigned as an event handler.
                    try {
                        rObj.message = 'Info: type=' + message.type + ' tagName=' + (<any>message.target).tagName;
                    }
                    catch { }
                }
                else if (typeof message === 'object' && (<any>message)?.message) {
                    // If an error-object, Clone fails and .message cannot be serialized.
                    rObj = JSON.parse(JSON.stringify(message));
                    rObj.message = message.message;
                }
            }

            this.callback('OnTemplateEnded', rObj);
        }

        /**
         * Log a message to the player
         */
        public log(message: string | any, level: LogLevels = LogLevels.debug): void {
            const e = document.getElementById('LogOutput');

            if (e) {
                const lineDiv = document.createElement('div');
                lineDiv.textContent = message;
                e.appendChild(lineDiv);
            }

            if (typeof message !== 'string') {
                try {
                    message = JSON.stringify(message);
                }
                catch (err) {
                    message = '**MALFORMED MESSAGE**';
                    return;
                }
            }
            this.callback('OnLog', { message: message, level: level });
        }

        public setPersistentItem(key: string, val: any): void {
            try {
                if (val != null) {
                    sessionStorage.setItem(key, JSON.stringify(val));
                    return;
                }

                sessionStorage.removeItem(key);
            }
            catch { }
        }

        public getPersistentItem(key: string, defaultValue?: any): any {
            try {
                const valS = sessionStorage.getItem(key);

                if (typeof valS === 'string')
                    return JSON.parse(valS);
            }
            catch { }

            return defaultValue;
        }

        async setLayerProperties(labelOrId: string, properties: Partial<Mq.LayerProperties>): Promise<void> {
            if (this.jsPlayerCaps < 9)
                throw Error('This jsPlayer does not support setLayerProperties - please upgrade');
            const [success] = await this.queryPlayer('setLayerProperties', { labelOrId, properties });
            // ZZZ: Do something is the query responds, but unsuccessfully?
        }

        async getLayerProperties(labelOrId: string): Promise<Mq.LayerProperties> {
            if (this.jsPlayerCaps < 9)
                throw Error('This jsPlayer does not support getLayerProperties - please upgrade');
            const [success, layerProps] = await this.queryPlayer<Mq.LayerProperties>('getLayerProperties', { labelOrId });
            if (success)
                return layerProps;

            throw Error('Unable to get layer properties');
        }

        /**
         * Signal parent window that the template failed.
         */
        public templateError(message?: (string | Event | Error | OnPreloadErrorReturn)): void {

            let rObj: OnPreloadErrorReturn = {};
            if (message) {
                if (typeof message === 'string')
                    rObj.message = message;
                else if (message instanceof Event) {
                    // Auto generate message if message argument is an event object.
                    // Can be the case if function is assigned as an event handler.
                    try {
                        rObj.message = 'Info: type=' + message.type + ' tagName=' + (<any>message.target).tagName;
                    }
                    catch {}
                }
                else if (typeof message === 'object' && (<any>message)?.message) {
                    // If an error-object, Clone fails and .message cannot be serialized.
                    rObj = JSON.parse(JSON.stringify(message));
                    rObj.message = message.message;
                }
            }

            this.callback('OnPreloadError', rObj);
        }

        /**
         * Signal parent window we're ready to accept events
         */
        private cmdReady(expectedDataFormat: Mq.OnLoadCompleteReturn = 'htv'): void {
            this.callback('OnCmdReady', {
                apiVersion: this.apiVersion,
                classVersion: this.classVersion,
                format: expectedDataFormat
            } as OnCmdReadyReturn);
        }

        /**
         * Dispatch load event
         */
        private async onLoad(): Promise<void> {
            if (!this.viewImplements(this._view.onLoad))
                return this.cmdReady();

            try {
                const res = await this._view.onLoad(this);

                if (res !== false)
                    this.cmdReady(typeof res !== 'boolean' ? res : void 0);
            }
            catch {
                this.log("Error in onLoad()", LogLevels.error);
            }
        }

        /**
         * Dispatch metadata event
         */
        private onMetadata(metadata: Mq.PlayerMetadata): void {
            this.currentMetadata = metadata;
            this.jsPlayerCaps = Number(metadata.jsPlayerCapabilities ?? 0);

            if (this.viewImplements(this._view.onMetadata))
                this._view.onMetadata(metadata);
        }

        protected parseData(data: string): unknown {
            return JSON.parse(data);
        }

        public delay(ms: number, rejectOnEnd: true): Promise<never>;
        public delay(ms: number, rejectOnEnd: false): Promise<void>;
        public delay(ms: number, rejectOnEnd: boolean): Promise<void>;
        public delay(ms: number, rejectOnEnd: boolean = false): Promise<void | never> {
            return delay(ms, rejectOnEnd);
        }

        /**
         * Dispatch OnPreload event
         */
        private async onPreload(data: unknown): Promise<void> {

            this.currentData = data;

            try {
                let returnObj: Mq.OnPreloadReadyReturn | void | boolean;
                let haveCalled: boolean = false;

                if (this.isContentTemplateData(data)) {
                    const backup = data.__playerMetadata;
                    data.__playerMetadata = this.currentMetadata;

                    if (haveCalled = this.viewImplements(this._view.onPreloadWithContentTemplateData))
                        returnObj = await this._view.onPreloadWithContentTemplateData(data);
                    else if (haveCalled = this.viewImplements(this._view.onPreloadWithDataHelper) &&
                        (<any>Htv).DataHelper != null)
                        returnObj = await this._view.onPreloadWithDataHelper((<any>Htv).DataHelper.get(data));

                    data.__playerMetadata = backup;
                }

                if (!haveCalled) {
                    if (haveCalled = this.viewImplements(this._view.onPreload))
                        returnObj = await this._view.onPreload(data);
                    else if (haveCalled = this.viewImplements(this._view.onPreloadWithDataHelper) &&
                        (<any>Mq).DataHelper != null)
                        returnObj = await this._view.onPreloadWithDataHelper((<any>Mq).DataHelper.get(data, this.currentMetadata));
                }
                if (!haveCalled)
                    throw Error('Invalid data or a suitable "onPreload" method not implemented in template');

                // The template may return false explicitly to disable auto-preloadComplete():
                if (returnObj !== false) {
                    this.preloadComplete(typeof returnObj !== 'boolean' ? returnObj : void 0);
                }
            }
            catch (ex) {
                this.templateError(ex);
            }
        }

        isContentTemplateData(val: unknown): val is Htv.ContentTemplateData {

            return (val != null && typeof val === 'object' &&
                ((<any>val).data == null || Array.isArray((<any>val).data)) &&
                // ZZZ: The isArray here is a workaround for a bug in PlayerSim which
                // propagated into the Thumbs Generator. Remove at leisure:
                (Array.isArray((<any>val).metadata) ||
                    (typeof (<any>val).metadata === 'object' && (<any>val).metadata?.created)) &&
                (<any>val).template != null &&
                typeof (<any>val).template === 'object');
        }

        /**
         * Dispatch OnShift event
         */
        private async onShift(): Promise<void> {
            this.onShiftCount++;

            const capturedonShiftCount = this.onShiftCount;
            let onEndCalled = false;

            const onEnded = (msg?: (string | Event | OnTemplateEndedReturn), wasError: boolean = false): void => {
                if (!onEndCalled)
                    if (capturedonShiftCount === this.onShiftCount)
                        !wasError ? this.templateEnded(msg) : this.templateError(msg);
                    else
                        this.log(`Late TemplateEnded (intercepted and stopped): "${msg || ''}"`, LogLevels.warning);
                else
                    this.log('TemplateEnded called more than once');

                onEndCalled = true;
            };

            try {
                let res;
                if (this.viewImplements(this._view.onShift))
                    if (res = await this._view.onShift(onEnded))
                        onEnded(res);
            }
            catch (err) {
                onEnded(err, true);
            }
        }

        /**
         * Dipatch OnPlay event
         */
        private onPlay(): void {
            if (this.viewImplements(this._view.onPlay))
                this._view.onPlay();
        }

        /**
         * Dispatch OnPause event
         */
        private onPause(): void {
            if (this.viewImplements(this._view.onPause))
                this._view.onPause();
        }

        /**
         * Dispatch OnVolume
         */
        private onVolume(volume: number): void {
            if (this.viewImplements(this._view.onVolume))
                this._view.onVolume(volume);
        }

        /**
         * Dispatch OnQueueNext
         */
        private onQueueNext(queueInfo: QueueInfo): void {
            if (this.viewImplements(this._view.onQueueNext))
                this._view.onQueueNext(queueInfo);
        }

        /**
         * Dispatch custom editor message
         */
        private onEditorMessage(message: any): void {
            if (this.viewImplements(this._view.onEditorMessage))
                this._view.onEditorMessage(message);
        }

        /**
         * Dispatch changed attribute message
         */
        private onAttribute(attribute: any): void {
            if (this.viewImplements(this._view.onAttribute))
                this._view.onAttribute(attribute as Mq.UpdatedAttribute);
        }

        /**
         * Dispatch geo position coords
         */
        private onGeoPosition(geoCoords: any): void {
            if (this.viewImplements(this._view.onGeoPosition))
                this._view.onGeoPosition(geoCoords as Mq.GeoPosition);
        }

        /**
         * Send custom message to editor
         */
        public editorMessage(message: any): any {
            return this.callback('OnEditorMessage', message);
        }

        /**
         * Set global attribute
         */
        public setAttribute(attribute: Mq.Attribute, force: boolean = false): any {
            (attribute as UpdatedAttribute).force = force;
            return this.callback('OnSetAttribute', attribute);
        }

       /**
        * Send query to template player.
        * Response is handled by handleQueryResponse()
        */
        private queryPlayer<T>(name: string, params: any, timeoutMs: number = 2000): Promise<[boolean, T]> {

            const id = name.substr(0, 3) + '_' + Math.round(Math.random() * 1000000000).toString(36); // so we can identify the response

            return new Promise<[boolean, T]>((resolve, reject) => {
                setTimeout(() => {
                    delete this._queries[id];
                    reject(Error('OnQuery timed out'));
                }, timeoutMs);

                this._queries[id] = <PlayerQuery>{
                    id: id,
                    name: name,
                    params: params,
                    callback: (success: boolean, result: any) => resolve([success, result])
                };

                this.callback('OnQuery', { id: id, name: name, params: params });
            });
        }

        /**
         * Handle queryPlayer() response from MqTemplatePlayer
         */
        private handleQueryResponse(res: any): void {

            const query = this._queries[res.id];
            query?.callback?.(res.result, res.response);
            delete this._queries[res.id];
        }

        /**
         * Get persistent item that is scoped to the current channel.
         */
        public async getPersistentChannelItem<T>(key: string, defaultValue?: T): Promise<T> {

            const [success, res] = await Promise.race([
                this.queryPlayer('getPersistentItem', { key: key }),
                this.delay(2000, true)
            ]) as [boolean, T];

            return success ? res : defaultValue;
        }

        /**
         * Set persistent item that is scoped to the current channel.
         */
        public async setPersistentChannelItem(key: string, value: any): Promise<void> {

            await Promise.race([
                this.queryPlayer('setPersistentItem', { key: key, value: value }),
                this.delay(2000, true)
            ]);
        }

        /**
         * Internal debugging
         */
        private debug(str: string): void {
            console.log(str);
        }

        /**
         * Set global proof-of-play extra data.
         * Per item extra data can be set via preloadComplete()/templateEnded()
         */
        public setGlobalProofExtraData(data: any): any {
            return this.callback('OnSetGlobalProofExtraData', data);
        }

        /**
         * Log a Proof of play event.
         */
        public logProofOfPlay(params: ProofOfPlayLogEntry): void {
            (<any>params).mediaId = params.id;
            delete params.id;

            this.queryPlayer('logProofOfPlay', params);
        }

        #appConfig: AppConfig<any>;

        /**
         * Get the AppConfig, if any. Otherwise null.
         * This method will return the same on each call (after a successful population!).
         */
        async getConfig<T extends Mq.TemplateConfig>(checkData?: (data: any) => data is T): Promise<T>;
        async getConfig<T extends Mq.TemplateConfig>(options?: {
            verifyData?: (data: any) => data is T;
            url?: string}): Promise<T>;
        async getConfig<T extends Mq.TemplateConfig>(
            checkDataOrOptions?: ((data: any) => data is T) | {
                verifyData?: (data: any) => data is T;
                url?: string}): Promise<T> {

            if (!this.#appConfig || !this.#appConfig.config) {
                const url = (<any>checkDataOrOptions)?.url ?? void 0;
                const checkData = !checkDataOrOptions
                    ? void 0 : typeof checkDataOrOptions == 'function'
                        ? checkDataOrOptions
                        : checkDataOrOptions.verifyData ?? void 0;

                this.#appConfig = new Mq.AppConfig<T>(url);
                await this.#appConfig.load(checkData);
            }

            return this.#appConfig.config as T ?? null;
        }
    }

    export function delay(ms: number, rejectOnEnd: true): Promise<never>;
    export function delay(ms: number, rejectOnEnd: false): Promise<void>;
    export function delay(ms: number, rejectOnEnd: boolean): Promise<void | never>;
    export function delay(ms: number, rejectOnEnd: boolean = false): Promise <void | never> {
        return new Promise((resolve, reject) => setTimeout(() => {
            if (rejectOnEnd)
                reject('Timed out');
            else
                resolve();
        }, ms));
    }
}
