/// <reference path="../HtvModel.d.ts" />
/// <reference path="./TemplateController.d.ts" />

declare namespace Mq.Async {

    interface TemplateView {
        onLoad?(controller?: Mq.Async.ITemplateController): Promise<Mq.OnLoadCompleteReturn>;

        onMetadata?(metadata: Mq.PlayerMetadata): void;

        onPreload?(data: unknown): Promise<Mq.OnPreloadReadyReturn | void | boolean>;
        onPreloadWithContentTemplateData?(data: Htv.ContentTemplateData): Promise<Mq.OnPreloadReadyReturn | void | boolean>;
        onPreloadWithDataHelper?(data: Mq.IDataHelper): Promise<Mq.OnPreloadReadyReturn | void | boolean>;

        onShift?(onEnd?: (msg?: string | Event | OnTemplateEndedReturn) => void): Promise<boolean | void | OnTemplateEndedReturn | string>;
        onPlay?(): void;
        onPause?(): void;
        onVolume?(volume: number): void;
        onQueueNext?(info: Mq.QueueInfo): void;
        onEditorMessage?(message: any): void;
        onAttribute?(attribute: Mq.UpdatedAttribute): void;
        onGeoPosition?(geoCoords: Mq.GeoPosition): void;
    }
}
