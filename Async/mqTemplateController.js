var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
/// <reference path="./HtvModel.d.ts" />
/// <reference path="./DataHelper.d.ts" />
var Mq;
(function (Mq) {
    class PlayerData {
        constructor(metadata) {
            this.metadata = metadata;
            this.layoutName = metadata.playerId;
            this.capabilities = metadata.jsPlayerCapabilities || 0;
            this.viewCount = metadata.viewCount;
            this.volume = metadata.volume;
            this.geoPosition = metadata.geoPosition;
            this.layout = metadata.channelLayer;
        }
        get viewMode() {
            if (this.cachedViewMode)
                return this.cachedViewMode;
            if (this.metadata.thumbnailRecordingMode)
                return this.cachedViewMode = "thumb" /* Mq.ViewMode.Thumbnail */;
            if (this.metadata.viewMode == "preview" /* Mq.PlayerViewMode.Preview */)
                return this.cachedViewMode = "preview" /* Mq.ViewMode.Preview */;
            if (this.metadata.viewMode == "edit" /* Mq.PlayerViewMode.Edit */)
                return this.cachedViewMode = "edit" /* Mq.ViewMode.Edit */;
            if (this.metadata.__isDevHost)
                return this.cachedViewMode = "debug" /* Mq.ViewMode.Debug */;
            // Q: What if we're on Android or other embedded Player?
            if (typeof window.navigator.userAgent === 'string' &&
                window.navigator.userAgent.indexOf(' mqCef/') >= 0)
                return this.cachedViewMode = "local" /* Mq.ViewMode.LocalChannel */;
            return this.cachedViewMode = "server" /* Mq.ViewMode.WebChannel */;
        }
        get attributes() {
            if (this.cachedAttributes)
                return this.cachedAttributes;
            if (!this.metadata || !this.metadata.attributes)
                return this.cachedAttributes = {};
            this.cachedAttributes = this.metadata.attributes.reduce((o, v) => {
                o[v.name] = v;
                return o;
            }, {});
            const sets = {
                player: {},
                static: {},
                dyn: {},
                shared: {}
            };
            for (const attr of this.metadata.attributes) {
                const parts = attr.name.split('.');
                if (parts.length > 1) {
                    const coll = sets[parts[0]] || (sets[parts[0]] = {});
                    parts.shift();
                    coll[parts.join('.')] = attr;
                }
            }
            for (const key in sets)
                this.cachedAttributes[key] = sets[key];
            return this.cachedAttributes;
        }
    }
    Mq.PlayerData = PlayerData;
    class PlaylistData {
        constructor(metadata) {
            this.invocationCount = 0;
            this.priorityLevel = 0;
            this.isFirst = !!metadata.firstInPlaylist;
            this.isLast = !!metadata.lastInPlaylist;
            this.invocationCount = metadata.playlistInvocationCount || 0;
            this.priorityLevel = metadata.priorityLevel || 0;
        }
    }
    Mq.PlaylistData = PlaylistData;
    class TemplateData {
        constructor(metadata) {
            var _a;
            this.settings = (_a = metadata.pqiTags) !== null && _a !== void 0 ? _a : {};
        }
    }
    class DataHelper {
        constructor(rawData, playerMetadata) {
            this.rawData = rawData;
            this.playerMetadata = playerMetadata;
            this.type = 'op';
            this.content = rawData;
            this.channel = rawData === null || rawData === void 0 ? void 0 : rawData.channel;
            this.client = playerMetadata.htv
                ? {
                    id: playerMetadata.htv.clientId,
                    channel: playerMetadata.htv.channel
                }
                : {
                    // Old or newer Player.
                    id: (playerMetadata === null || playerMetadata === void 0 ? void 0 : playerMetadata.htvClientId) || (playerMetadata === null || playerMetadata === void 0 ? void 0 : playerMetadata.playbackDeviceId),
                    channel: rawData === null || rawData === void 0 ? void 0 : rawData.channel
                };
        }
        get template() {
            return this.cachedTemplate || (this.cachedTemplate = new TemplateData(this.playerMetadata));
        }
        get player() {
            return this.cachedPlayer || (this.cachedPlayer = new PlayerData(this.playerMetadata));
        }
        get playlist() {
            return this.cachedPlaylist || (this.cachedPlaylist = new PlaylistData(this.playerMetadata));
        }
        static isAttribute(v) {
            return v && (typeof v === 'object') && v.name && v.active && (typeof v.name === 'string') && (typeof v.active === 'boolean');
        }
        static get(data, playerMetadata) {
            return DataHelper.cachedData !== data
                ? (DataHelper.cachedHelper = new DataHelper(DataHelper.cachedData = data, playerMetadata))
                : DataHelper.cachedHelper;
        }
        static getNonCached(data, playerMetadata) {
            return DataHelper.cachedData !== data
                ? new DataHelper(data, playerMetadata)
                : DataHelper.cachedHelper;
        }
        getDuration(defaultSeconds = 10) {
            var _a, _b;
            const acceptMetadataDurationModes = ["edit" /* Mq.ViewMode.Edit */, "debug" /* Mq.ViewMode.Debug */];
            const res = this.player.metadata.duration ||
                (acceptMetadataDurationModes.some(vm => vm === this.player.viewMode) && ((_b = (_a = this.content) === null || _a === void 0 ? void 0 : _a.metadata) === null || _b === void 0 ? void 0 : _b._duration)) ||
                defaultSeconds;
            return Number(res);
        }
    }
    Mq.DataHelper = DataHelper;
})(Mq || (Mq = {}));
/// <reference path="./HtvModel.d.ts" />
/// <reference path="./DataHelper.d.ts" />
var Htv;
(function (Htv) {
    let DataHelperData;
    (function (DataHelperData) {
        function filterByFieldType(arrayOfFieldMaps, fieldTypesOrSelector, transform) {
            if (!arrayOfFieldMaps)
                return {};
            let includeField;
            if (typeof fieldTypesOrSelector === 'function')
                includeField = fieldTypesOrSelector;
            else if (Array.isArray(fieldTypesOrSelector))
                includeField = (f) => fieldTypesOrSelector.some((v) => f.type === v);
            else
                includeField = (f) => fieldTypesOrSelector == f.type;
            const subset = arrayOfFieldMaps.reduce((a, v) => {
                let ia = {};
                // tslint:disable-next-line:forin
                for (const key in v) {
                    const f = v[key];
                    if (includeField(f)) {
                        if (!DataHelper.isMediaField(f)) {
                            if (f.value != null)
                                ia[key] = transform ? transform(f.value) : f.value;
                        }
                        else
                            ia[key] = (f.files && f.files.length > 0) ? f.files : void 0;
                    }
                }
                a.push(ia);
                return a;
            }, []);
            const res = {};
            if (subset.length > 0) {
                for (const key in subset[0])
                    res[key] = subset[0][key];
                for (let i = 0; i < subset.length; i++)
                    res[i] = subset[i];
            }
            return res;
        }
        // ZZZ: Needs interface etc.
        class ContentData {
            constructor(_data) {
                this._data = _data;
                this.id = _data.id;
                this.name = _data.name;
                this.fields = _data.data;
                this.duration = _data.duration != null ? _data.duration : undefined;
                this.metadata = _data.metadata ? _data.metadata : {};
            }
            get texts() {
                return this.cachedTextFieldValues ||
                    (this.cachedTextFieldValues = filterByFieldType(this._data.data, "text" /* Htv.FieldTypes.Text */, String));
            }
            get xml() {
                return this.cachedXmlFieldValues ||
                    (this.cachedXmlFieldValues = filterByFieldType(this._data.data, "xmlData" /* Htv.FieldTypes.XmlData */, String));
            }
            get geodata() {
                return this.cachedGeodataFieldValues ||
                    (this.cachedGeodataFieldValues = filterByFieldType(this._data.data, "geodata" /* Htv.FieldTypes.GeoData */, String));
            }
            get numbers() {
                return this.cachedNumberFieldValues ||
                    (this.cachedNumberFieldValues = filterByFieldType(this._data.data, ["number" /* Htv.FieldTypes.Number */,
                        "integer" /* Htv.FieldTypes.Integer */,
                        "float" /* Htv.FieldTypes.Float */,
                        "double" /* Htv.FieldTypes.Double */], Number));
            }
            get enums() {
                return this.cachedEnumFieldValues ||
                    (this.cachedEnumFieldValues = filterByFieldType(this._data.data, "enum" /* Htv.FieldTypes.Enum */, Number));
            }
            get enumStrings() {
                return this.cachedEnumStringFieldValues ||
                    (this.cachedEnumStringFieldValues = filterByFieldType(this._data.data, "enum" /* Htv.FieldTypes.Enum */, String));
            }
            get medias() {
                return this.cachedmediaFieldValues ||
                    (this.cachedmediaFieldValues = filterByFieldType(this._data.data, "files" /* Htv.FieldTypes.Files */));
            }
            get nonMedia() {
                return this.cachedNonMediaFieldValues ||
                    (this.cachedNonMediaFieldValues =
                        filterByFieldType(this._data.data, (f) => f.type != "files" /* Htv.FieldTypes.Files */, String));
            }
            static parseDateTime(s) {
                if (this.isIsoDateTimeIsh.test(s))
                    s = s.replace(' ', 'T');
                if (!this.isIsoDateTime.test(s) && !this.isDateTime.test(s)) {
                    if (this.isTime.test(s))
                        s = '1970-01-01T' + s;
                    else if (this.isAmPmTime.test(s))
                        s = 'Jan 1, 1970, ' + s;
                    else if (this.isIsoDate.test(s))
                        s += 'T00:00:00';
                    else if (this.isDate.test(s))
                        s += ', 12:00 am';
                }
                return new Date(s);
            }
            get dateTimes() {
                return this.cachedDateTimeFieldValues ||
                    (this.cachedDateTimeFieldValues = filterByFieldType(this._data.data, "dateTime" /* Htv.FieldTypes.DateTime */, (v) => {
                        const d = ContentData.parseDateTime(v);
                        return (d && !isNaN(d.getTime())) ? d : void 0;
                    }));
            }
            get dates() {
                return this.cachedDateFieldValues ||
                    (this.cachedDateFieldValues = filterByFieldType(this._data.data, "date" /* Htv.FieldTypes.Date */, (v) => {
                        const d = ContentData.parseDateTime(v);
                        if (d && !isNaN(d.getTime())) {
                            d.setHours(0, 0, 0, 0);
                            return d;
                        }
                        return void 0;
                    }));
            }
            get times() {
                return this.cachedTimeFieldValues ||
                    (this.cachedTimeFieldValues = filterByFieldType(this._data.data, "dateTime" /* Htv.FieldTypes.DateTime */, (v) => {
                        const d = ContentData.parseDateTime(v);
                        if (d && !isNaN(d.getTime())) {
                            d.setFullYear(1970, 0, 1);
                            return d;
                        }
                        return void 0;
                    }));
            }
        }
        ContentData.isDateTime = /^\w+ ?\d{1,2}, *\d{4}, *\d{1,2}:\d{2}[\d:\.]* *(am|pm)?$/;
        ContentData.isIsoDateTime = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}(?::\d{2}(?:\.\d{1,3})?)?$/;
        ContentData.isIsoDateTimeIsh = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}(?::\d{2}(?:\.\d{1,3})?)?$/;
        ContentData.isDate = /^\w+ ?\d{1,2}, *\d{4}$/;
        ContentData.isIsoDate = /^\d{4}-\d{2}-\d{2}$/;
        ContentData.isAmPmTime = /^\d{1,2}:\d{2}[\d:\.]* *(am|pm)$/;
        ContentData.isTime = /^\d{2}:\d{2}(?::\d{2}(?:\.\d{1,3})?)?$/;
        DataHelperData.ContentData = ContentData;
        class TemplateData extends ContentData {
            constructor(data, settings) {
                super(data);
                this.settings = settings ? settings : {};
            }
        }
        DataHelperData.TemplateData = TemplateData;
    })(DataHelperData = Htv.DataHelperData || (Htv.DataHelperData = {}));
    class DataHelper {
        constructor(rawData, playerMetadata) {
            var _a, _b, _c, _d, _e, _f, _g;
            this.rawData = rawData;
            this.type = 'htv';
            if (playerMetadata)
                rawData.__playerMetadata = playerMetadata;
            this.channel = rawData === null || rawData === void 0 ? void 0 : rawData.channel;
            this.client = ((_a = rawData.__playerMetadata) === null || _a === void 0 ? void 0 : _a.htv)
                ? {
                    id: (_c = (_b = rawData.__playerMetadata) === null || _b === void 0 ? void 0 : _b.htv) === null || _c === void 0 ? void 0 : _c.clientId,
                    channel: (_e = (_d = rawData.__playerMetadata) === null || _d === void 0 ? void 0 : _d.htv) === null || _e === void 0 ? void 0 : _e.channel
                }
                : {
                    // Old Player.
                    id: ((_f = rawData.__playerMetadata) === null || _f === void 0 ? void 0 : _f.htvClientId) || ((_g = rawData.__playerMetadata) === null || _g === void 0 ? void 0 : _g.playbackDeviceId),
                    channel: rawData === null || rawData === void 0 ? void 0 : rawData.channel
                };
        }
        get content() {
            return this.cachedContent || (this.cachedContent = new Htv.DataHelperData.ContentData(this.rawData));
        }
        get template() {
            return this.cachedTemplate || (this.cachedTemplate = new Htv.DataHelperData.TemplateData(this.rawData.template, this.rawData.settings));
        }
        get player() {
            return this.cachedPlayer || (this.cachedPlayer = new Mq.PlayerData(this.rawData.__playerMetadata));
        }
        get playlist() {
            return this.cachedPlaylist || (this.cachedPlaylist = new Mq.PlaylistData(this.rawData.__playerMetadata));
        }
        static isAttribute(v) {
            return v && (typeof v === 'object') && v.name && v.active && (typeof v.name === 'string') && (typeof v.active === 'boolean');
        }
        static isOtherField(f) {
            return f && (f.type !== 'files');
        }
        static isMediaField(f) {
            return f && (f.type === 'files');
        }
        static get(data, playerMetadata) {
            return DataHelper.cachedData !== data
                ? (DataHelper.cachedHelper = new DataHelper(DataHelper.cachedData = data, playerMetadata))
                : DataHelper.cachedHelper;
        }
        static getNonCached(data, playerMetadata) {
            return DataHelper.cachedData !== data
                ? new DataHelper(data, playerMetadata)
                : DataHelper.cachedHelper;
        }
        getDuration(defaultSeconds = 10) {
            return Number(this.rawData.duration ||
                this.rawData.template.duration ||
                (this.player.metadata.duration != 600
                    ? this.player.metadata.duration : null) ||
                defaultSeconds);
        }
        getExpiry(defaultExpiryMinutes = 0) {
            try {
                let expiryMinutes = this.template.numbers.ExpiryMinutes;
                if (Number(expiryMinutes) <= 0 && defaultExpiryMinutes > 0)
                    expiryMinutes = defaultExpiryMinutes;
                if (Number(expiryMinutes) > 0)
                    return new Date(new Date(this.content.metadata.created).getTime() + Number(expiryMinutes) * 60000);
            }
            catch (_a) { }
            return new Date(3000, 0, 1);
        }
        getFieldValue(fieldName, defaultVal, index = 0) {
            try {
                if (this.rawData && this.rawData.data && this.rawData.data[index]) {
                    let field = this.rawData.data[index][fieldName];
                    if (DataHelper.isOtherField(field))
                        return field.value;
                    else if (DataHelper.isMediaField(field)) {
                        let files = this.getMediaFieldFiles(fieldName, index);
                        if (files && files.length > 0)
                            return files[0].url;
                    }
                }
            }
            catch (_a) { }
            return defaultVal;
        }
        getMediaFieldFiles(fieldName, index = 0) {
            try {
                if (this.rawData && this.rawData.data && this.rawData.data[index]) {
                    let field = this.rawData.data[index][fieldName];
                    if (DataHelper.isMediaField(field))
                        return field.files;
                }
            }
            catch (_a) { }
            return [];
        }
        getSettingValue(settingName, defaultVal) {
            if (this.rawData && this.rawData.settings) {
                let val = this.rawData.settings[settingName];
                return 'undefined' === typeof val ? defaultVal : val;
            }
            return defaultVal;
        }
    }
    Htv.DataHelper = DataHelper;
})(Htv || (Htv = {}));
var Mq;
(function (Mq) {
    var _AppConfig_config;
    class AppConfig {
        constructor(url = 'config.json') {
            this.url = url;
            _AppConfig_config.set(this, void 0);
            this.url += '?t=' + Math.round(Math.random() * 1000000).toString(36);
        }
        async load(checkData) {
            var _a, _b;
            if (!__classPrivateFieldGet(this, _AppConfig_config, "f"))
                try {
                    const resp = await fetch(this.url);
                    let config;
                    if (resp.ok &&
                        (config = await resp.json()) &&
                        this.isAppConfig(config) &&
                        ((_a = checkData === null || checkData === void 0 ? void 0 : checkData(config)) !== null && _a !== void 0 ? _a : true)) {
                        __classPrivateFieldSet(this, _AppConfig_config, config, "f");
                    }
                }
                catch (_c) { }
            if (!__classPrivateFieldGet(this, _AppConfig_config, "f")) {
                // Workaround for the case where we're hosted on file: (typically Tizen):
                const iFrame = document.createElement('iframe');
                iFrame.style.display = 'none';
                document.body.appendChild(iFrame);
                try {
                    const waitForLoad = new Promise((resolve, reject) => {
                        iFrame.onload = resolve;
                        iFrame.onerror = reject;
                    });
                    iFrame.src = this.url;
                    await Promise.race([
                        waitForLoad,
                        Mq.Async.delay(2000, true)
                    ]);
                    const doc = iFrame.contentWindow.document;
                    const text = doc.body.children[0].textContent;
                    const config = JSON.parse(text);
                    if (this.isAppConfig(config) &&
                        ((_b = checkData === null || checkData === void 0 ? void 0 : checkData(config)) !== null && _b !== void 0 ? _b : true)) {
                        __classPrivateFieldSet(this, _AppConfig_config, config, "f");
                    }
                }
                catch (_d) { }
                finally {
                    iFrame === null || iFrame === void 0 ? void 0 : iFrame.remove();
                }
            }
            return this;
        }
        get config() {
            return __classPrivateFieldGet(this, _AppConfig_config, "f");
        }
        isAppConfig(possibleConfig) {
            return possibleConfig && typeof possibleConfig === 'object' &&
                possibleConfig.type === 'AppConfig';
        }
    }
    _AppConfig_config = new WeakMap();
    Mq.AppConfig = AppConfig;
})(Mq || (Mq = {}));
/// <reference path="./TemplateController.d.ts" />
/// <reference path="./TemplateView.d.ts" />
var Mq;
(function (Mq) {
    var Async;
    (function (Async) {
        var _TemplateController_appConfig;
        /**
         * Template controller
         */
        class TemplateController {
            constructor(view, minimizeLogNoise = false) {
                var _a;
                this.minimizeLogNoise = minimizeLogNoise;
                this._view = null;
                this.currentMetadata = null;
                this.currentData = null;
                this._playState = 0 /* PlayState.Playing */;
                this._queries = {};
                this.onShiftCount = 0;
                // Version of the jsPlayer <> MqTemplate API
                this.apiVersion = 9;
                // MqTemplate version
                // Version history:
                //    3 : 2015-??-??
                //    4 : 2015-12-17 - added onQueueNext, added metadata.groupRelativePath and metadata.lastQueueNumber
                //    5 : 2016-11-08 - added onEditorMessage, editorMessage
                //    6 : 2017-06-29 - metadata.attributes, metadata.jsPlayerCapabilities, onAttribute, setAttribute, onGeoPosition,
                //                     preloadComplete params duration, rejectPlay and suggestedNextPreload
                //    7 : 2017-06-30 - query api: setPersistentItem, getPersistentItem   
                //    8 : 2019-03-06 - added setGlobalProofExtraData
                //    9 : 2020-08-14 - added logProofOfPlay
                this.classVersion = 9;
                _TemplateController_appConfig.set(this, void 0);
                this._view = view ? view : {};
                this.parentWindow = window !== window.parent ? window.parent : null;
                // Get event token from get param token=
                this.token = ((_a = window.location.search.match(/[?&]token=([^&#]+)/)) === null || _a === void 0 ? void 0 : _a[1]) || 'default-token';
                // Listen to messages posted by parent window
                window.addEventListener('message', (ev) => {
                    const message = ev.data;
                    if (!message || typeof message !== 'object') {
                        return;
                    }
                    // Should be a jsPlayerEvent
                    if (message.type !== 'jsPlayerEvent') {
                        if (!this.minimizeLogNoise)
                            this.debug('Event ignored:  event=' + message.event + ' type="' + message.type + '"');
                        return;
                    }
                    //this.debug('Got event=' + message.event + ' type=' + message.type);
                    // Handle events
                    switch (message.event) {
                        // Metadata from player
                        case 'OnMetadata':
                            this.onMetadata(message.data);
                            break;
                        // Preload data
                        case 'OnPreload':
                            let parsedData = null;
                            try {
                                // Call the onParse method. Default action is JSON.parse
                                parsedData = this.parseData(message.data);
                            }
                            catch (err) {
                                // Failed to parse - can't continue
                                this.templateError('onParse failed: ' + err.message + ' data: ' + message.data);
                                break; // Note: early break
                            }
                            this.onPreload(parsedData);
                            break;
                        // Show/shift template
                        case 'OnShift':
                            // Default playState after OnShift event is playing
                            this._playState = 0 /* PlayState.Playing */;
                            this.onShift();
                            break;
                        // Play template (if not already playing)
                        case 'OnPlay':
                            switch (this._playState) {
                                case 0 /* PlayState.Playing */:
                                    this.debug('OnPlay event ignored (already playing)');
                                    break;
                                default:
                                    this._playState = 0 /* PlayState.Playing */;
                                    this.onPlay();
                                    break;
                            }
                            break;
                        // Pause template (if not already paused)
                        case 'OnPause':
                            switch (this._playState) {
                                case 1 /* PlayState.Paused */:
                                    this.debug('OnPause event ignored (already paused)');
                                    break;
                                default:
                                    this._playState = 1 /* PlayState.Paused */;
                                    this.onPause();
                                    break;
                            }
                            break;
                        case 'OnVolume':
                            this.onVolume(Number(message.data.volume));
                            break;
                        case 'OnQueueNext':
                            this.onQueueNext(message.data);
                            break;
                        case 'OnEditorMessage':
                            this.onEditorMessage(message.data);
                            break;
                        // On schedule attribute set
                        case 'OnAttribute':
                            this.onAttribute(message.data);
                            break;
                        // On geo position coords
                        case 'OnGeoPosition':
                            this.onGeoPosition(message.data);
                            break;
                        case 'OnQueryResponse':
                            this.handleQueryResponse(message.data);
                            break;
                        default:
                            if (!this.minimizeLogNoise)
                                this.debug('Unknown event=' + message.event + ' type="' + message.type + '"');
                            break;
                    }
                }, false);
                if (document.readyState == 'complete')
                    this.onLoad();
                else
                    document.addEventListener('DOMContentLoaded', () => this.onLoad(), false);
            }
            viewImplements(possibleFunc) {
                return typeof possibleFunc === 'function';
            }
            /**
             * Post a message to the parent window
             */
            callback(ev, data) {
                if (!this.parentWindow)
                    return console.error(`Cannot postMessage() event ${ev}. No parent window found!`);
                if (!this.parentWindow.postMessage)
                    throw Error('postMessage() not available');
                return this.parentWindow.postMessage({
                    type: 'PubliqEvent',
                    event: ev,
                    data: data,
                    token: this.token,
                }, '*');
            }
            /**
            * Request a screenshot (in thumbs-mode).
            */
            triggerScreenshot() {
                this.callback('OnTriggerScreenshot');
            }
            /**
            * Signal parent window that we're ready to receive shift event
            */
            preloadComplete(properties = null) {
                this.callback('OnPreloadComplete', properties);
            }
            /**
             * Signal parent window that template has ended
             */
            templateEnded(message) {
                let rObj = {};
                if (message) {
                    if (typeof message === 'string')
                        rObj.message = message;
                    else if (message instanceof Event) {
                        // Auto generate message if message argument is an event object.
                        // Can be the case if function is assigned as an event handler.
                        try {
                            rObj.message = 'Info: type=' + message.type + ' tagName=' + message.target.tagName;
                        }
                        catch (_a) { }
                    }
                    else if (typeof message === 'object' && (message === null || message === void 0 ? void 0 : message.message)) {
                        // If an error-object, Clone fails and .message cannot be serialized.
                        rObj = JSON.parse(JSON.stringify(message));
                        rObj.message = message.message;
                    }
                }
                this.callback('OnTemplateEnded', rObj);
            }
            /**
             * Log a message to the player
             */
            log(message, level = "debug" /* LogLevels.debug */) {
                const e = document.getElementById('LogOutput');
                if (e) {
                    const lineDiv = document.createElement('div');
                    lineDiv.textContent = message;
                    e.appendChild(lineDiv);
                }
                if (typeof message !== 'string') {
                    try {
                        message = JSON.stringify(message);
                    }
                    catch (err) {
                        message = '**MALFORMED MESSAGE**';
                        return;
                    }
                }
                this.callback('OnLog', { message: message, level: level });
            }
            setPersistentItem(key, val) {
                try {
                    if (val != null) {
                        sessionStorage.setItem(key, JSON.stringify(val));
                        return;
                    }
                    sessionStorage.removeItem(key);
                }
                catch (_a) { }
            }
            getPersistentItem(key, defaultValue) {
                try {
                    const valS = sessionStorage.getItem(key);
                    if (typeof valS === 'string')
                        return JSON.parse(valS);
                }
                catch (_a) { }
                return defaultValue;
            }
            /**
             * Signal parent window that the template failed.
             */
            templateError(message) {
                let rObj = {};
                if (message) {
                    if (typeof message === 'string')
                        rObj.message = message;
                    else if (message instanceof Event) {
                        // Auto generate message if message argument is an event object.
                        // Can be the case if function is assigned as an event handler.
                        try {
                            rObj.message = 'Info: type=' + message.type + ' tagName=' + message.target.tagName;
                        }
                        catch (_a) { }
                    }
                    else if (typeof message === 'object' && (message === null || message === void 0 ? void 0 : message.message)) {
                        // If an error-object, Clone fails and .message cannot be serialized.
                        rObj = JSON.parse(JSON.stringify(message));
                        rObj.message = message.message;
                    }
                }
                this.callback('OnPreloadError', rObj);
            }
            /**
             * Signal parent window we're ready to accept events
             */
            cmdReady(expectedDataFormat = 'htv') {
                this.callback('OnCmdReady', {
                    apiVersion: this.apiVersion,
                    classVersion: this.classVersion,
                    format: expectedDataFormat
                });
            }
            /**
             * Dispatch load event
             */
            async onLoad() {
                if (!this.viewImplements(this._view.onLoad))
                    return this.cmdReady();
                try {
                    const res = await this._view.onLoad(this);
                    if (res !== false)
                        this.cmdReady(typeof res !== 'boolean' ? res : void 0);
                }
                catch (_a) {
                    this.log("Error in onLoad()", "error" /* LogLevels.error */);
                }
            }
            /**
             * Dispatch metadata event
             */
            onMetadata(metadata) {
                this.currentMetadata = metadata;
                if (this.viewImplements(this._view.onMetadata))
                    this._view.onMetadata(metadata);
            }
            parseData(data) {
                return JSON.parse(data);
            }
            delay(ms, rejectOnEnd = false) {
                return delay(ms, rejectOnEnd);
            }
            /**
             * Dispatch OnPreload event
             */
            async onPreload(data) {
                this.currentData = data;
                try {
                    let returnObj;
                    let haveCalled = false;
                    if (this.isContentTemplateData(data)) {
                        const backup = data.__playerMetadata;
                        data.__playerMetadata = this.currentMetadata;
                        if (haveCalled = this.viewImplements(this._view.onPreloadWithContentTemplateData))
                            returnObj = await this._view.onPreloadWithContentTemplateData(data);
                        else if (haveCalled = this.viewImplements(this._view.onPreloadWithDataHelper) &&
                            Htv.DataHelper != null)
                            returnObj = await this._view.onPreloadWithDataHelper(Htv.DataHelper.get(data));
                        data.__playerMetadata = backup;
                    }
                    if (!haveCalled) {
                        if (haveCalled = this.viewImplements(this._view.onPreload))
                            returnObj = await this._view.onPreload(data);
                        else if (haveCalled = this.viewImplements(this._view.onPreloadWithDataHelper) &&
                            Mq.DataHelper != null)
                            returnObj = await this._view.onPreloadWithDataHelper(Mq.DataHelper.get(data, this.currentMetadata));
                    }
                    if (!haveCalled)
                        throw Error('Invalid data or a suitable "onPreload" method not implemented in template');
                    // The template may return false explicitly to disable auto-preloadComplete():
                    if (returnObj !== false) {
                        this.preloadComplete(typeof returnObj !== 'boolean' ? returnObj : void 0);
                    }
                }
                catch (ex) {
                    this.templateError(ex);
                }
            }
            isContentTemplateData(val) {
                var _a;
                return (val != null && typeof val === 'object' &&
                    (val.data == null || Array.isArray(val.data)) &&
                    // ZZZ: The isArray here is a workaround for a bug in PlayerSim which
                    // propagated into the Thumbs Generator. Remove at leisure:
                    (Array.isArray(val.metadata) ||
                        (typeof val.metadata === 'object' && ((_a = val.metadata) === null || _a === void 0 ? void 0 : _a.created))) &&
                    val.template != null &&
                    typeof val.template === 'object');
            }
            /**
             * Dispatch OnShift event
             */
            async onShift() {
                this.onShiftCount++;
                const capturedonShiftCount = this.onShiftCount;
                let onEndCalled = false;
                const onEnded = (msg, wasError = false) => {
                    if (!onEndCalled)
                        if (capturedonShiftCount === this.onShiftCount)
                            !wasError ? this.templateEnded(msg) : this.templateError(msg);
                        else
                            this.log(`Late TemplateEnded (intercepted and stopped): "${msg || ''}"`, "warn" /* LogLevels.warning */);
                    else
                        this.log('TemplateEnded called more than once');
                    onEndCalled = true;
                };
                try {
                    let res;
                    if (this.viewImplements(this._view.onShift))
                        if (res = await this._view.onShift(onEnded))
                            onEnded(res);
                }
                catch (err) {
                    onEnded(err, true);
                }
            }
            /**
             * Dipatch OnPlay event
             */
            onPlay() {
                if (this.viewImplements(this._view.onPlay))
                    this._view.onPlay();
            }
            /**
             * Dispatch OnPause event
             */
            onPause() {
                if (this.viewImplements(this._view.onPause))
                    this._view.onPause();
            }
            /**
             * Dispatch OnVolume
             */
            onVolume(volume) {
                if (this.viewImplements(this._view.onVolume))
                    this._view.onVolume(volume);
            }
            /**
             * Dispatch OnQueueNext
             */
            onQueueNext(queueInfo) {
                if (this.viewImplements(this._view.onQueueNext))
                    this._view.onQueueNext(queueInfo);
            }
            /**
             * Dispatch custom editor message
             */
            onEditorMessage(message) {
                if (this.viewImplements(this._view.onEditorMessage))
                    this._view.onEditorMessage(message);
            }
            /**
             * Dispatch changed attribute message
             */
            onAttribute(attribute) {
                if (this.viewImplements(this._view.onAttribute))
                    this._view.onAttribute(attribute);
            }
            /**
             * Dispatch geo position coords
             */
            onGeoPosition(geoCoords) {
                if (this.viewImplements(this._view.onGeoPosition))
                    this._view.onGeoPosition(geoCoords);
            }
            /**
             * Send custom message to editor
             */
            editorMessage(message) {
                return this.callback('OnEditorMessage', message);
            }
            /**
             * Set global attribute
             */
            setAttribute(attribute, force = false) {
                attribute.force = force;
                return this.callback('OnSetAttribute', attribute);
            }
            /**
             * Send query to template player.
             * Response is handled by handleQueryResponse()
             */
            queryPlayer(name, params) {
                const id = name.substr(0, 3) + '_' + Math.round(Math.random() * 1000000000).toString(36); // so we can identify the response
                return new Promise((resolve, reject) => {
                    this._queries[id] = {
                        id: id,
                        name: name,
                        params: params,
                        callback: (success, result) => resolve([success, result])
                    };
                    this.callback('OnQuery', { id: id, name: name, params: params });
                });
            }
            /**
             * Handle queryPlayer() response from MqTemplatePlayer
             */
            handleQueryResponse(res) {
                var _a;
                const query = this._queries[res.id];
                (_a = query === null || query === void 0 ? void 0 : query.callback) === null || _a === void 0 ? void 0 : _a.call(query, res.result, res.response);
                delete this._queries[res.id];
            }
            /**
             * Get persistent item that is scoped to the current channel.
             */
            async getPersistentChannelItem(key, defaultValue) {
                const [success, res] = await Promise.race([
                    this.queryPlayer('getPersistentItem', { key: key }),
                    this.delay(2000, true)
                ]);
                return success ? res : defaultValue;
            }
            /**
             * Set persistent item that is scoped to the current channel.
             */
            async setPersistentChannelItem(key, value) {
                await Promise.race([
                    this.queryPlayer('setPersistentItem', { key: key, value: value }),
                    this.delay(2000, true)
                ]);
            }
            /**
             * Internal debugging
             */
            debug(str) {
                console.log(str);
            }
            /**
             * Set global proof-of-play extra data.
             * Per item extra data can be set via preloadComplete()/templateEnded()
             */
            setGlobalProofExtraData(data) {
                return this.callback('OnSetGlobalProofExtraData', data);
            }
            /**
             * Log a Proof of play event.
             */
            logProofOfPlay(params) {
                params.mediaId = params.id;
                delete params.id;
                this.queryPlayer('logProofOfPlay', params);
            }
            async getConfig(checkDataOrOptions) {
                var _a, _b, _c;
                if (!__classPrivateFieldGet(this, _TemplateController_appConfig, "f") || !__classPrivateFieldGet(this, _TemplateController_appConfig, "f").config) {
                    const url = (_a = checkDataOrOptions === null || checkDataOrOptions === void 0 ? void 0 : checkDataOrOptions.url) !== null && _a !== void 0 ? _a : void 0;
                    const checkData = !checkDataOrOptions
                        ? void 0 : typeof checkDataOrOptions == 'function'
                        ? checkDataOrOptions
                        : (_b = checkDataOrOptions.verifyData) !== null && _b !== void 0 ? _b : void 0;
                    __classPrivateFieldSet(this, _TemplateController_appConfig, new Mq.AppConfig(url), "f");
                    await __classPrivateFieldGet(this, _TemplateController_appConfig, "f").load(checkData);
                }
                return (_c = __classPrivateFieldGet(this, _TemplateController_appConfig, "f").config) !== null && _c !== void 0 ? _c : null;
            }
        }
        _TemplateController_appConfig = new WeakMap();
        Async.TemplateController = TemplateController;
        function delay(ms, rejectOnEnd = false) {
            return new Promise((resolve, reject) => setTimeout(() => {
                if (rejectOnEnd)
                    reject('Timed out');
                else
                    resolve();
            }, ms));
        }
        Async.delay = delay;
    })(Async = Mq.Async || (Mq.Async = {}));
})(Mq || (Mq = {}));
//# sourceMappingURL=mqTemplateController.js.map