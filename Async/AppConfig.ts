namespace Mq {

    export interface TemplateConfig {
        type: 'AppConfig';
        version?: number;
    }

    export class AppConfig<T extends TemplateConfig> {
        #config: T;

        constructor(
            private url: string = 'config.json') {
            this.url += '?t=' + Math.round(Math.random() * 1000000).toString(36);
        }

        async load(checkData?: (data: any) => data is T): Promise<AppConfig<T>> {
            if (!this.#config)
                try {
                    const resp = await fetch(this.url);
                    let config: T | any;

                    if (resp.ok &&
                        (config = await resp.json()) &&
                        this.isAppConfig(config) &&
                        (checkData?.(config) ?? true)) {

                        this.#config = config;
                    }
                }
                catch { }

            if (!this.#config) {
                // Workaround for the case where we're hosted on file: (typically Tizen):
                const iFrame = document.createElement('iframe') as HTMLIFrameElement;
                iFrame.style.display = 'none';
                document.body.appendChild(iFrame);

                try {
                    const waitForLoad = new Promise((resolve, reject) => {
                        iFrame.onload = resolve;
                        iFrame.onerror = reject;
                    });
                    iFrame.src = this.url;

                    await Promise.race([
                        waitForLoad,
                        Async.delay(2000, true)]);

                    const doc = iFrame.contentWindow.document;
                    const text = doc.body.children[0].textContent;
                    const config: T | any = JSON.parse(text);

                    if (this.isAppConfig(config) &&
                        (checkData?.(config) ?? true)) {

                        this.#config = config;
                    }
                }
                catch { }
                finally {
                    iFrame?.remove();
                }
            }

            return this;
        }

        public get config(): T {
            return this.#config;
        }

        private isAppConfig(possibleConfig: any): possibleConfig is T {
            return possibleConfig && typeof possibleConfig === 'object' &&
                possibleConfig.type === 'AppConfig';
        }
    }
}
