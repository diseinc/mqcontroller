/// <reference path="./TemplateView.d.ts" />
/// <reference path="./TemplateController.d.ts" />
/// <reference path="../DataHelper.ts" />

namespace Mq {

    const enum TemplateState {
        none = 0,
        onLoad,
        onMetadata,
        onPreload,
        onShift,
        ended
    }

    export abstract class SimpleViewBase implements Async.TemplateView {
        protected mqController: Mq.Async.ITemplateController;
        #state: TemplateState = TemplateState.none;

        #onEnd: (msg?: string | Event | OnTemplateEndedReturn) => void;
        #preloadComplete: (res: Mq.OnPreloadReadyReturn | void) => void;
        #startPlay: () => void;

        async onLoad(controller: Mq.Async.ITemplateController): Promise<Mq.OnLoadCompleteReturn> {
            this.mqController = controller;
            this.#state = TemplateState.onLoad;
        }

        async onPreloadWithDataHelper(helper: Mq.IDataHelper): Promise<Mq.OnPreloadReadyReturn | void> {
            if (this.#state !== TemplateState.onLoad && this.#state !== TemplateState.onMetadata)
                throw Error('SimpleTemplate: OnPreload called more than once. Please update MultiQ Player or disable re-use.');

            this.#state = TemplateState.onPreload;

            if (!this.main)
                throw Error('main() not implemented');

            try {
                const preloadCompletePromise = new Promise<Mq.OnPreloadReadyReturn | void>((resolve, reject) => {
                    this.#preloadComplete = (res) => resolve({
                        ...(res ?? {}),
                        ...{ reuseTemplate: false }
                    });
                }).then(v => (this.#preloadComplete = null, v));

                const mainResPromise = this.main(helper, this.mqController)
                    .then(v => {
                        // If main returns before onShift, we reject playback
                        if (this.#state === TemplateState.onPreload)
                            this.#preloadComplete?.({ rejectPlay: true });
                        // A truthy result means we should force-end.
                        else if (v && this.#state === TemplateState.onShift)
                            this.#onEnd?.(typeof v !== 'boolean' ? v : void 0);
                        this.#onEnd = null;
                        // Suppress our own completion. If we're in onShift,
                        // nobody is listening anyway...
                        return preloadCompletePromise;
                    })
                    .catch(err => {
                        // If we fail before onShift, let the outer catch handle it...
                        if (this.#state === TemplateState.onPreload)
                            throw err;
                        // Otherwise, we'll have to fail explicitly.
                        this.mqController.templateError(err);
                    });

                const res = await Promise.race([
                    mainResPromise,
                    preloadCompletePromise
                ]);

                if (this.#state === TemplateState.onPreload)
                    return res;
            }
            catch (err) {
                if (this.#state === TemplateState.onPreload)
                    throw err;

                //this.#onEnd?.(err);
                // Don't exit.
                await Promise.race([]);
            }
        }

        async onShift(onEnd: (msg?: string | Event | OnTemplateEndedReturn) => void): Promise<void> {
            this.#state = TemplateState.onShift;
            this.#onEnd = () => {
                this.#state = TemplateState.ended;
                onEnd();
            };
            this.#startPlay?.();
        }

        abstract main(helper: Mq.IDataHelper, mqController: Mq.Async.ITemplateController): Promise<void | boolean | string>;

        protected async waitForPlay(preloadReturn?: Mq.OnPreloadReadyReturn): Promise<void> {
            this.#preloadComplete?.(preloadReturn);
            // Hibernate until onShift.
            return new Promise(resolve => {
                this.#startPlay = () => resolve();
            });
        }
    }
}
