export interface StringMap<T> {
    [K: string]: T;
}

export type TemplateSettings = StringMap<any>;

type DataModelFormats = 'htv' | 'op';

export interface OnCmdReadyReturn {
    apiVersion: number;
    classVersion: number;
    format?: DataModelFormats;
}

export type OnLoadCompleteReturn = DataModelFormats | void | boolean;

interface CommonReturn {
    proofExtraData?: any;
}
export interface OnPreloadReadyReturn extends CommonReturn {
    duration?: number;
    suggestedNextPreload?: number;

    // In edit-mode, the template can inform the editor of its duration-restrictions
    // (causing the value in the UI to be updated). If both are set to the same value,
    // the time-picker control in the UI will have user-input disabled.
    // Ignored by jsPlayer.
    cms?: {
        minDuration?: number;
        maxDuration?: number;
    };

    rejectPlay?: boolean;
    hasAudio?: boolean;
    useCustomScreenshotEvent?: boolean;
    screenshotTime?: number;
    enableProofLogging?: boolean;

    reuseTemplate?: boolean;
    scaling?: true;
    /** In the syntax [width]x[height] e.g. 1920x1080 */
    resolution?: string;
}

export interface OnTemplateEndedReturn extends CommonReturn {
    message?: string;
    forceNew?: boolean;
}

export interface OnPreloadErrorReturn {
    message?: string;
}

export const enum LogLevels {
    debug = 'debug',
    info = 'info',
    warning = 'warn',
    error = 'error',
}

export interface ProofOfPlayLogEntry {
    id: string;
    duration?: number;
    segmentId?: string;
    file?: string;
}

export interface LayerProperties {
    top: number;
    left: number;
    width: number;
    height: number;
    background: string; // style.background
    pointerEvents: boolean;
    scale: boolean;
    visible: boolean;
}

export interface Attribute {
    name: string;
    active: boolean;
    data?: string;
    label?: string;
}

export interface UpdatedAttribute extends Attribute {
    force?: boolean;
    shared?: boolean;
    origin?: string;
    master?: string;
    type?: string;
    revision?: number;
    changed?: Array<keyof Attribute>;
}

// ZZZ: Coordinates does not exist anymore? Have to investigate later...
interface ObsoleteDOMCoordinates {
    readonly accuracy: number;
    readonly altitude: number | null;
    readonly altitudeAccuracy: number | null;
    readonly heading: number | null;
    readonly latitude: number;
    readonly longitude: number;
    readonly speed: number | null;
}

// ZZZ: used to be "extends Partial<Coordinates>"
export interface GeoPosition extends Partial<ObsoleteDOMCoordinates> {
    valid?: boolean;
    numberSatellites?: number;
}

export interface ChannelInfo {
    id: string;
    name: string;
    lastModified: string;
}

export const enum PlayerViewMode {
    Edit = 'edit',
    Preview = 'preview',
}

export interface QueueInfo {
    desk: number;
    line: string;
    number: number;
    metadata: {
        servicetype: number;
        tickettype: number;
        zeros: number;
    };
}

export interface Layout {
    type?: 'player' | 'slavePlayer';
    mode?: 'normal' | 'dataPlaylist';
    top?: number;
    left?: number;
    width?: number;
    height?: number;
    //layer?: number;
    label?: string;
}

interface HtvSpecific {
    channel?: ChannelInfo;
    clientId?: string;
}

export interface PlayerMetadata {
    jsPlayerCapabilities?: number;
    viewCount: number;
    viewMode?: PlayerViewMode;
    volume: number;
    groupRelativePath: string;
    lastQueueNumber: any;
    pqiTags: TemplateSettings;
    attributes: Attribute[];
    geoPosition: GeoPosition;
    firstInPlaylist?: boolean;
    lastInPlaylist?: boolean;
    playlistInvocationCount?: number;
    priorityLevel?: number;
    playerId?: string; // Layout-id (scene)
    // Obsolete, moved to htv.clientId:
    htvClientId?: string;
    htv?: HtvSpecific;
    thumbnailRecordingMode?: boolean;

    syncGroup?: {
        role: 'slave' | 'master';
        masterHostInstanceId: string;
        slaveHostInstanceId: string;
        masterPlayerId: string;
    }

    duration?: number;
    playbackDeviceId?: string; // For OP.

    channelLayer?: Layout;

    __isDevHost?: boolean;
}
