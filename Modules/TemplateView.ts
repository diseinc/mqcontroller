import type { ITemplateController } from "./TemplateController";
import type {
  GeoPosition,
  OnLoadCompleteReturn,
  OnPreloadReadyReturn,
  OnTemplateEndedReturn,
  PlayerMetadata, QueueInfo, UpdatedAttribute
} from "./MqModel";
import type { ContentTemplateData } from "./HtvModel";
import type { IDataHelper } from "./DataHelper";

export interface ITemplateView {
    onLoad?(controller?: ITemplateController): Promise<OnLoadCompleteReturn>;

    onMetadata?(metadata: PlayerMetadata): void;

    onPreload?(data: unknown): Promise<OnPreloadReadyReturn | void | boolean>;
    onPreloadWithContentTemplateData?(data: ContentTemplateData): Promise<OnPreloadReadyReturn | void | boolean>;
    onPreloadWithDataHelper?(data: IDataHelper): Promise<OnPreloadReadyReturn | void | boolean>;

    onShift?(onEnd?: (msg?: string | Event | OnTemplateEndedReturn) => void): Promise<boolean | void | OnTemplateEndedReturn | string>;
    onPlay?(): void;
    onPause?(): void;
    onVolume?(volume: number): void;
    onQueueNext?(info: QueueInfo): void;
    onEditorMessage?(message: any): void;
    onAttribute?(attribute: UpdatedAttribute): void;
    onGeoPosition?(geoCoords: GeoPosition): void;
}

export class TemplateViewBase implements ITemplateView {
    protected mqController: ITemplateController | undefined;

    async onLoad(controller?: ITemplateController): Promise<OnLoadCompleteReturn> {
        this.mqController = controller;
    }
}
