import { AspectMode  } from "./Media.Data.js";
export type MediaHandlerType = ('video' | 'picture');

export interface IMediaHandler {
    onEnded?: () => void;
    preload(url: string): Promise<number>;

    fallbackDuration: number;
    mediaDuration?: number;
    silent: boolean;
    stillframeTime?: number;

    type: MediaHandlerType;
    start(): Promise<void>;
    // ZZZ: Document stages.
    getStartStages(): (() => void)[];
    remove(): void;

    setVolume(volume: number): void;
}

export interface IVideoHandler extends IMediaHandler {
    type: 'video';
    videoElement: HTMLVideoElement | null;
}
export interface IPictureHandler extends IMediaHandler {
    type: 'picture';
    imgElement: HTMLImageElement | null;
}

export type MediaHandler = IVideoHandler | IPictureHandler;

export abstract class BaseMediaHandler {
    protected container?: HTMLElement;

    private isEnded: boolean = false;
    protected isRemoved = false;

    public abstract type: MediaHandlerType;

    public abstract fallbackDuration: number;
    public abstract mediaDuration?: number;
    public silent: boolean = false;
    public abstract stillframeTime?: number;

    /**
     * Callback for media ended or timeout
     */
    public onEnded?: () => void;

    constructor(protected fadeInTime: number, protected fadeOutTime?: number) {
    }

    protected callEnd(): void {
        if (!this.isEnded && !this.isRemoved) {
            this.isEnded = true;

            this.onEnded?.();
        }
    }

    protected activateFadeIn(): void {
        if (this.fadeInTime && this.container) {
            this.container.style.animationDuration = this.fadeInTime + 's';
            this.container.classList.add('intro')
        }
    }

    protected activateFadeOut(): void {
        if (this.fadeOutTime && this.container) {
            this.container.style.animationDuration = this.fadeOutTime + 's';
            this.container.classList.add('outro')
        }
    }

    protected remove(): void {
        if (!this.isRemoved) {
            this.isRemoved = true;
            if (this.container?.parentElement)
                this.container.parentElement?.removeChild(this.container);
            this.container = undefined;
        }
    }

    static aspectModeFromString(name: string): number {
        name = name.toUpperCase();

        for (let v in AspectMode)
            if (v.toUpperCase() === name)
                return Number(AspectMode[v]);

        return AspectMode.undefined;
    }

    static matchesExtension(name: string, supportedExtensions: string[] = []): boolean {
        name = /(.*)(\?|$)+/.exec(name ?? '')?.[1] ?? '';
        const lastDot = name.lastIndexOf('.');

        if (lastDot < 0 || supportedExtensions.length === 0)
            return false;

        const ext = name.substring(lastDot + 1).toLowerCase();

        return supportedExtensions.some((v) => v == ext);
    }
}

type ConnectCallback = (element: HTMLElement, handlerType: MediaHandlerType) => HTMLElement;

interface RegisteredHandler {
    priority: number;
    canPlay: (url: string, type?: string) => boolean;
    create(connectAsHiddenContainer: ConnectCallback,
        fadeInTime?: number, fadeOutTime?: number): MediaHandler;
}

const handlers: RegisteredHandler[] = [];

export function createAppropriateHandler(params: {connectAsHiddenContainer: ConnectCallback,
    fadeInTime?: number, fadeOutTime?: number},
    url: string, type?: string) {

    return handlers.find((h) => h.canPlay(url, type))?.create(params.connectAsHiddenContainer, params.fadeInTime, params.fadeOutTime)
}

export function registerHandler(info: RegisteredHandler): void {
    handlers.push(info);
    handlers.sort((info1, info2) => info2.priority - info1.priority);
}
