import * as Mq from "../MqModel.js";
import * as Htv from "../HtvModel.js";

export const enum MediaType {
    Video = 'Video',
    Picture = 'Picture',
    Audio = 'Audio'
}

/**
 * With the "normal" ordering - some templates may differ.
 * Note that the actual names will be used as CSS-class specifiers,
 * so beware of changing them... You may break the world.
 */
export enum AspectMode {
    undefined = -1,
    fitWidth = 0,
    fitHeight,
    letterbox,
    crop,
    stretch
}

export interface MediaPreloadInfo {
    mediaType?: MediaType;
    url: string;
    mimeType?: string;
    duration?: number;
    mediaDuration?: number;
    width?: number;
    height?: number;
    aspect?: number;
    volume?: number;

    aspectMode?: AspectMode;

    fadeInDuration?: number;
    fadeOutDuration?: number;

    hideUntilStart?: boolean;

    muted?: boolean;

    stillFrameTime?: number;

    videoScale?: [xScale: number, yScale: number];
}

export function getPreOrSuffixes(
    templateSettings: Mq.StringMap<any>,
    attributes: Mq.StringMap<Mq.Attribute>,
    singleValSetting: string,
    multiValSetting: string,
    multiValIndex: number,
    attributeLookupSetting: string): string {

    if (multiValSetting) {
        const val = templateSettings[multiValSetting];

        if (val)
            if (typeof val === 'string')
                return String(val);
            else if (Array.isArray(val) && (<any[]>val).length >= 1)
                return String(val[multiValIndex % val.length]);
    }

    const val: any = templateSettings[singleValSetting];

    if (typeof val === 'string')
        return String(val);

    // Fallback to attribute-setting, typically from a static-attr in the channel.
    if (attributeLookupSetting && attributes) {
        const val = templateSettings[attributeLookupSetting];

        if (typeof val === 'string') {
            const attr = attributes[val];

            if (attr && attr.data)
                return attr.data;
        }
    }

    return '';
}

export function getMediaAspect(media: Htv.ContentFile): number {
    const w: number = media?.properties?.['width'];
    const h: number = media?.properties?.['height'];
    let res: number = 0;

    if (w && h) {
        const pAspect = media?.properties?.['pixelAspect'] || 1;
        res = w * pAspect / h;
    }

    return res;
}

