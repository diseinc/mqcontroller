import {
    BaseMediaHandler,
    IPictureHandler,
    MediaHandlerType,
    registerHandler } from "../MediaHandlers.js";
import * as Data from "../Media.Data.js";
import { isIE } from "../Media.js";

const mediaType: Data.MediaType = Data.MediaType.Picture;
const priority: number = 100;

const supportedExtensions = [
    'jpg', 'png', 'jpeg',
    'gif', 'bmp', 'tif',
    'tiff', 'svg', 'webp', 'avif',
];
const supportedMimeTypes = [
    'image/jpeg',
    'image/png', 'image/apng', 'image/x-png',
    'image/gif', 'image/bmp',
    'image/tiff', 'image/svg+xml',
    'image/webp', 'image/avif',
];

/**
 * Handles picture preload and animation sequence.
 */
export class PictureHandler extends BaseMediaHandler implements IPictureHandler {
    type: 'picture' = 'picture';
    public imgElement: HTMLImageElement | null;
    private duration: number = 0;
    public fallbackDuration: number = 0;
    public mediaDuration?: number;
    public silent: boolean = true;
    public stillframeTime?: number;

    constructor(private connectAsHiddenContainer: (element: HTMLElement, handlerType: MediaHandlerType) => HTMLElement,
        fadeInTime?: number, fadeOutTime?: number) {

        super(fadeInTime ?? 0, fadeOutTime);

        this.imgElement = document.createElement('img');
        this.imgElement.classList.add('picture');
    }

    /**
     * Preload picture and setup event listeners
     */
    preload(url: string): Promise<number> {
        if (!this.imgElement)
            throw Error();

        this.container = this.connectAsHiddenContainer(this.imgElement, this.type);
        this.duration = this.fallbackDuration;

        // Promise resolves when preload is complete
        let promise = new Promise<number>((resolve, reject) => {
            if (!this.imgElement)
                throw Error();

            this.container?.addEventListener('animationend',
                (ev: AnimationEvent) => {
                    if (ev.animationName === 'outro')
                        this.callEnd();
                });

            // Callback when picture is ready
            this.imgElement.onload = () => resolve(this.duration);

            // Reject on error
            this.imgElement.onerror = (e: Event | string) => {
                reject(Error('Picture error handler fired'));
                if (this.imgElement)
                    this.imgElement.onerror = null;
            };

            this.imgElement.src = url;
        });

        return promise;
    }

    start(): Promise<void> {
        if (!this.container)
            throw Error();

        this.container.style.visibility = "inherit";
        this.activateFadeIn();

        setTimeout(() => this.callEnd(), this.duration * 1000);

        // Handle outro - delay it slightly to avoid flashing in certain situations
        if (this.fadeOutTime && this.fadeOutTime > 0)
            setTimeout(() => this.activateFadeOut(),
                (this.duration - this.fadeOutTime + (2 / 60)) * 1000);

        return Promise.resolve();
    }

    getStartStages(): (() => void)[] {
        return [
            () => { return; },
            () => this.start()
        ];
    }

    setVolume(volume: number): void { }

    remove(): void {
        if (this.isRemoved)
            return;

        if (this.imgElement) {
            this.imgElement.onerror = null;
            this.imgElement.onload = null;
        }

        super.remove();
        this.imgElement = null;
    }
}

function canPlayContent(url: string, type?: string) {
    const lcType = type?.toLowerCase()
    return type === mediaType ||
        (lcType && supportedMimeTypes.some(mt => mt == lcType)) ||
        (!type && BaseMediaHandler.matchesExtension(url, supportedExtensions));
}

registerHandler({
    priority: priority,
    canPlay: (url: string, type?: string) => canPlayContent(url, type),
    create: (connectAsHiddenContainer, fadeInTime, fadeOutTime) => new PictureHandler(connectAsHiddenContainer, fadeInTime, fadeOutTime),
});
