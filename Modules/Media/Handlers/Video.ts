import {
    BaseMediaHandler,
    IVideoHandler,
    MediaHandlerType,
    registerHandler } from "../MediaHandlers.js";
import * as Data from "../Media.Data.js";
import { isIE } from "../Media.js";

const mediaType: Data.MediaType = Data.MediaType.Video;
const priority: number = 10;

const supportedExtensions = [
    'mp4', 'webm', 'wmv',
    'mpg', 'm2p', 'm2v',
    'mov', 'm4v', 'mpg', 'mpeg',
    'avi',
    'ogg' // <-- ???: Also pure audio?
];
const supportedMimeTypes = [
    'video/mp4', 'video/webm',
    'video/mpeg', 'video/mp2p',
    'video/quicktime',
    'video/ms-wmv', 'video/x-ms-wmv',
    'video/msvideo', 'video/x-msvideo',
    'video/ogg'
];

/**
 * Handles video preload and animation sequence.
 */
export class VideoHandler extends BaseMediaHandler implements IVideoHandler {
    type: 'video' = 'video';
    public videoElement: HTMLVideoElement | null;
    private duration?: number;
    public fallbackDuration: number = 10;
    public mediaDuration?: number;
    public silent: boolean = false;
    private volume?: number;
    public stillframeTime?: number;
    protected isStarted: boolean = false;

    constructor(private connectAsHiddenContainer: (element: HTMLElement, handlerType: MediaHandlerType) => HTMLElement,
        fadeInTime?: number, fadeOutTime?: number) {

        super(fadeInTime ?? 0, fadeOutTime);

        this.videoElement = document.createElement('video');
        this.videoElement.muted = true;
        this.videoElement.classList.add('video');
    }

    /**
     * Preload video and setup event listeners
     */
    preload(url: string): Promise<number> {
        if (!this.videoElement)
            throw Error();

        this.container = this.connectAsHiddenContainer(this.videoElement, this.type);

        if (this.silent)
            this.setVolume(0);

        // Promise resolves when preload is complete
        return new Promise<number>((resolve, reject) => {
            if (!this.videoElement)
                throw Error();

            this.videoElement.src = url;
            this.videoElement.controls = false;
            this.videoElement.preload = 'auto';

            // Listen to animationend events
            this.container?.addEventListener('animationend',
                (ev: AnimationEvent) => {
                    if (ev.animationName === 'outro')
                        this.callEnd();
                });

            // Reject on error
            this.videoElement.onerror = (e: Event | string) => {
                if (!this.videoElement)
                    throw Error();

                this.videoElement.onerror = null;

                // console.log('Video error: reject: ' + e.type);
                console.log("Error " + this.videoElement?.error?.code + "; details: " + ((<any>this.videoElement).error?.message ?? ""));

                reject(Error('Video error handler fired'));
            };

            let haveAddedOutro: boolean = !this.fadeOutTime;

            const onTimeUpdate = () => {
                if (!this.videoElement)
                    throw Error();

                const v = this.videoElement;

                try {
                    if (!v || isNaN(v.duration) || this.duration == null)
                        return;

                    // Check if we should add the outro class
                    let remains = this.duration - v.currentTime;

                    if (!Number.isNaN(remains) && remains <= 0) {
                        this.videoElement.ontimeupdate = null;
                        this.videoElement?.removeEventListener('timeupdate', onTimeUpdate);
                        this.callEnd();
                    }

                    if (!haveAddedOutro && !isNaN(remains) && remains < (this.fadeOutTime ?? 0)) {
                        this.activateFadeOut();
                        haveAddedOutro = true;
                    }
                }
                catch (e) {
                    // ??? We'd better close up shop.
                    this.videoElement?.removeEventListener('timeupdate', onTimeUpdate);
                    this.callEnd();
                }
            };

            const onCanPlayThrough = () => {
                if (!this.videoElement)
                    throw Error();

                this.videoElement.removeEventListener('canplaythrough', onCanPlayThrough);
                this.videoElement.onerror = null;

                this.duration = (this.videoElement?.duration ?? 0) > 0
                    ? this.videoElement.duration
                    : (this.mediaDuration || this.fallbackDuration);

                // Handle outro
                this.videoElement.addEventListener('timeupdate', onTimeUpdate);

                // Workaround for Android:
                this.videoElement.play();
                this.videoElement.pause();

                // Loaded ok - resolve
                resolve(this.duration);
            };

            // Callback when video is ready
            this.videoElement.addEventListener('canplaythrough', onCanPlayThrough);

            // Video ended
            this.videoElement.onended = () => {
                this.callEnd();
            };

            // Start video preloading
            this.videoElement.load();
        }).then((duration) => {
            if (!this.videoElement)
                throw Error();

            if (typeof this.stillframeTime !== 'number' || !this.videoElement?.seekable)
                return duration;

            this.videoElement.pause();
            let snapPos = this.stillframeTime;
            if (snapPos >= duration)
                snapPos = duration * 0.1;

            return new Promise((resolve, reject) => {
                if (!this.videoElement)
                    throw Error();

                this.videoElement.onerror = (e: Event | string) => {
                    if (this.videoElement)
                        this.videoElement.onerror = null;

                    reject(Error('Video error handler fired'));
                };

                this.videoElement.onseeked = (ev) => {
                    if (this.videoElement)
                        this.videoElement.onseeked =
                        this.videoElement.onerror = null;

                    resolve(duration);
                };
                this.videoElement.currentTime = snapPos;
            });
        });
    }

    start(): Promise<void> {
        if (!this.videoElement)
            throw Error();

        this.isStarted = true;
        if (!this.silent)
            this.setVolume(this.volume ?? 1);

        this.activateFadeIn();
        if (this.container)
            this.container.style.visibility = "inherit";
        if (this.stillframeTime == null) {
            if (!isIE())
                return this.videoElement.play()
                    .catch(() => {
                        if (!this.videoElement)
                            return void 0;
                        this.videoElement.muted = true;
                        const clickHandler = () => {
                            this.setVolume(this.volume ?? 1);
                            this.videoElement?.removeEventListener('click', clickHandler);
                        };
                        this.videoElement.addEventListener('click', clickHandler);
                        return this.videoElement.play();
                    });

            this.videoElement.play();
        }
        else
            setTimeout(() => this.callEnd(), (this.duration ?? 10) * 1000);

        return Promise.resolve();
    }

    getStartStages(): (() => void)[] {
        return [
            () => this.videoElement?.play(),
            () => {
                this.activateFadeIn();
                if (this.container)
                    this.container.style.visibility = "inherit";
            }
        ];
    }

    setVolume(volume?: number): void {
        this.volume = volume;

        if (!this.isStarted)
            return;

        if (this.videoElement && volume != null) {
            const paused = this.videoElement.paused;

            this.videoElement.volume = this.silent ? 0 : volume;
            this.videoElement.muted = this.silent || volume === 0;

            // Modern standard browsers will pause the video if
            // the user haven't interacted with the document:
            if (!paused && this.videoElement.paused) {
                this.videoElement.muted = true;
                this.videoElement.play();
            }
        }
    }

    /**
     * Unload video
     */
    remove(): void {
        if (this.isRemoved || !this.videoElement)
            // Second call to this function - ignore
            return;

        // Unregister event handlers
        this.videoElement.onerror =
        this.videoElement.oncanplay =
        this.videoElement.oncanplaythrough =
        this.videoElement.onended =
        this.videoElement.ontimeupdate =
        this.videoElement.onstalled = null;

        this.videoElement.volume = 0; // mute

        // this.video.src = 'data:video/mp4;base64,===';
        this.videoElement.pause();
        this.videoElement.src = ''; // flickering?
        this.videoElement.load();

        if (this.videoElement.parentElement)
            this.videoElement.parentElement.removeChild(this.videoElement);
        super.remove();
        this.videoElement = null;
    }
}

function canPlayContent(url: string, type?: string) {
    const lcType = type?.toLowerCase()
    return type === mediaType ||
        (lcType && supportedMimeTypes.some(mt => mt == lcType)) ||
        (!type && BaseMediaHandler.matchesExtension(url, supportedExtensions));
}

registerHandler({
    priority: priority,
    canPlay: (url: string, type?: string) => canPlayContent(url, type),
    create: (connectAsHiddenContainer, fadeInTime, fadeOutTime) => new VideoHandler(connectAsHiddenContainer, fadeInTime, fadeOutTime),
});
