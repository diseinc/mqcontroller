//import * as Mq from "../MqModel.js";
import * as Data from "./Media.Data.js"
import { MediaHandler, createAppropriateHandler } from "./MediaHandlers.js";

// ZZZ: Move to Mq.Utility or something:
export function isTizen(): boolean {
    return !!navigator.userAgent.match(/Tizen/i);
}
export function isAndroid(): boolean {
    return !!navigator.userAgent.match(/Android/i);
}
export function isIE(): boolean {
    return /MSIE |Trident/.test(navigator.userAgent);
}

function getMediaContainer(container: HTMLElement, createAsHidden: boolean = true): HTMLElement {
    let div = document.createElement('div');
    if (createAsHidden)
        div.style.visibility = "hidden";
    container.appendChild(div);
    return div;
}

export function addMediaForPreload(container: HTMLElement,
    mediaInfo: Data.MediaPreloadInfo,
    duration?: number): MediaHandler {

    const mi: Data.MediaPreloadInfo = mediaInfo;

    duration = mi.duration;

    if (!mi.aspect && mi.width && mi.height)
        mi.aspect = mi.width / mi.height;

    let handler: MediaHandler | undefined = createAppropriateHandler(
        {
            connectAsHiddenContainer: (element, handlerType) => {
                if (handlerType === 'video')
                    return addVideo(getMediaContainer(container, mi.hideUntilStart ?? true),
                        element, mi.aspectMode ?? Data.AspectMode.letterbox, mi.aspect, mi.videoScale);
                return addPicture(getMediaContainer(container, mi.hideUntilStart ?? true),
                    element, mi.aspectMode ?? Data.AspectMode.letterbox, mi.aspect, mi.url);
            },
            fadeInTime: mi.fadeInDuration,
            fadeOutTime: mi.fadeOutDuration,
        }, mi.url, mi.mimeType || mi.mediaType);

    if (!handler)
        throw new TypeError("Unsupported media-type " + mi.mediaType);

    if (typeof duration === 'number')
        handler.fallbackDuration = duration;
    if (typeof mi.volume === 'number')
        handler.setVolume(mi.volume);
    handler.mediaDuration = mi.mediaDuration;
    handler.silent = mi.muted ?? false;
    handler.stillframeTime = mi.stillFrameTime;

    return handler;
}

export function preloadMedia(container: HTMLElement,
    mediaInfo: Data.MediaPreloadInfo,
    duration?: number): Promise<[MediaHandler, number]> {

    let mi: Data.MediaPreloadInfo;

    mi = mediaInfo;
    duration = mi.duration;

    const mediaHandler: MediaHandler = addMediaForPreload(container, mi, duration);

    return mediaHandler.preload(mi.url)
        .then((duration) => [mediaHandler, duration]);
}

function isPreloadMediaInfo(data: any): data is Data.MediaPreloadInfo {

    return data && typeof data === 'object' &&
        typeof data.url === 'string' &&
        data.name !== null &&
        typeof data.name !== 'string' &&
        typeof data.properties !== 'object';
}

function addVideo(container: HTMLElement,
    video: HTMLElement | HTMLVideoElement,
    aspectMode: Data.AspectMode, mediaAspect: number = 0,
    videoScale?: [xScale: number, yScale: number]): HTMLElement {

    container.appendChild(video);
    video.classList.add(Data.AspectMode[aspectMode]);

    inheritCSSClasses(video, container);

    container.classList.add("mediaContainer");

    const setScale = (aspect: number): void => {
        const containerAspect = container.clientWidth / container.clientHeight;
        if (Math.abs(containerAspect - aspect) > 0.001 || videoScale) {

            const wider: boolean = aspect > containerAspect;
            const scale = wider ? aspect / containerAspect : containerAspect / aspect;

            const scaleX = (aspectMode === Data.AspectMode.crop) ||
                (wider && aspectMode === Data.AspectMode.fitHeight) ||
                (!wider && ((aspectMode === Data.AspectMode.fitWidth) || (aspectMode === Data.AspectMode.stretch)));
            const scaleY = (aspectMode === Data.AspectMode.crop) ||
                (!wider && aspectMode === Data.AspectMode.fitWidth) ||
                (wider && ((aspectMode === Data.AspectMode.fitHeight) || (aspectMode === Data.AspectMode.stretch)));
            let transformScaleX = scaleX ? scale : 1;
            let transformScaleY = scaleY ? scale : 1;

            if (videoScale) {
                transformScaleX *= videoScale[0];
                transformScaleY *= videoScale[1];
            }

            //// Alternative:
            // let transformScaleX = 1, transformScaleY = 1;

            // if (wider)
            //    switch (aspectMode) {
            //        case AspectMode.crop:
            //        case AspectMode.fitHeight:
            //            transformScaleX = transformScaleY = scale;
            //            break;
            //        case AspectMode.stretch:
            //            transformScaleY = scale;
            //            break;
            //    }
            // else
            //    switch (aspectMode) {
            //        case AspectMode.crop:
            //        case AspectMode.fitWidth:
            //            transformScaleX = transformScaleY = scale;
            //            break;
            //        case AspectMode.stretch:
            //            transformScaleX = scale;
            //            break;
            //    }

            if (transformScaleX != 1 || transformScaleY != 1)
                video.style.transform = `scale(${transformScaleX},${transformScaleY})`;
        }
    };

    if (aspectMode != Data.AspectMode.letterbox || videoScale)
        if (mediaAspect)
            setScale(mediaAspect);
        else {
            const onCanPlayThrough = () => {
                video.removeEventListener('canplaythrough', onCanPlayThrough);

                const w = (<HTMLVideoElement>video).videoWidth;
                const h = (<HTMLVideoElement>video).videoHeight;

                if (w && h)
                    setScale(w / h);
            };

            // Callback when video is ready
            video.addEventListener('canplaythrough', onCanPlayThrough);
        }

    return container;
}

function addPicture(container: HTMLElement,
    picture: HTMLElement | HTMLImageElement,
    aspectMode: Data.AspectMode, mediaAspect: number = 0, url: string): HTMLElement {

    container.appendChild(picture);
    picture.classList.add(Data.AspectMode[aspectMode]);

    switch (aspectMode) {
        case Data.AspectMode.crop:
        case Data.AspectMode.letterbox:
            picture.classList.add("doubleAxis");
            break;
        default:
            picture.classList.add("singleAxis");
            break;
    }

    inheritCSSClasses(picture, container);

    container.classList.add("mediaContainer");
    return container;
}

function inheritCSSClasses(source: HTMLElement, target: HTMLElement): void {
    for(let i = 0; i <source.classList.length; i++)
        target.classList.add(source.classList[i]);
}
