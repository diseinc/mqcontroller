import { ChannelInfo, PlayerMetadata, StringMap, TemplateSettings } from "./MqModel.js";

export const enum FieldTypes {
  Text = 'text',
  Number = 'number',
  Integer = 'integer',
  Float = 'float',
  Double = 'double',
  Enum = 'enum',
  GeoData = 'geodata',
  XmlData = 'xmlData',
  Files = 'files',
  DateTime = 'dateTime',
  Date = 'date',
  Time = 'time',
}

export interface ContentFile {
  url: string;
  name: string;
  properties: StringMap<any>;
}
export interface Field {
  type: FieldTypes;
}
export interface FilesField extends Field {
  files: ContentFile[];
}
export interface OtherField extends Field {
  value: any;
}

type FieldValue = FilesField | OtherField;

export interface BaseContentData {
  id: string;
  name: string;
  duration: number;
  metadata: StringMap<any>;
  data: Array<StringMap<FieldValue>>;
}
export interface TemplateData extends BaseContentData {}
export interface ContentTemplateData extends BaseContentData {
  template: TemplateData;
  settings: TemplateSettings;
  channel?: ChannelInfo;
  // Not part of standard jsPlayer data, added by controller from onMetadata:
  __playerMetadata?: PlayerMetadata;
}
