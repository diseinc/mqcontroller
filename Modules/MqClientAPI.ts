
type CefQuery = {
    request: string; // JSON-serialized object of method and parameters.
    persistent?: boolean;
    onSuccess: (event: string) => void;
    onFailure: (errorCode: number, errorMessage: string) => void;
}
type CallCefQuery = (query: CefQuery) => void;

// Polyfill for debugging purposes
const cefQuery: CallCefQuery = (window as any).cefQuery as CallCefQuery ??
    ((req: CefQuery) => console.log(`Dummy cefQuery: ${JSON.stringify(req)}`));

/**
 * Client API error
 */
class MqClientApiError extends Error {
    name = 'MqClientApiError';
    request: any;

    constructor(errorMessage: string, errorCode: number, originalRequest?: any) {
        super(`${errorMessage} (code=${errorCode})`);
        this.request = originalRequest;
    }
}

/**
 * Exposed API
 */
export function isHostedByMqCef(): boolean {
    return (typeof window.navigator.userAgent === 'string' &&
        window.navigator.userAgent.indexOf(' mqCef/') >= 0);
}

type MessageEventLike = { data: { message: string; } };

export function registerEventCallback(callback: (event: any) => void): Promise<MessageEventLike> {
    // Registers the onSuccess function as a permanent event-push callback function.

    // Return a promise
    return new Promise<MessageEventLike>((resolve, reject) => {
        // Perform API request
        cefQuery({
            // Create request string (json string)
            request: JSON.stringify({
                method: 'registerEventCallback',
                parameters: null,
            }),

            persistent: true, // the onSuccess fn is a persistent callback

            // Persistent callback
            onSuccess: (event) => {
                try {
                    callback(JSON.parse(event));
                }
                catch (err) {
                    const msg = 'Exception in cefQuery.onSuccess (persistent callback) in mqClientApi(registerEventCallback). Response=' + event + ' Error=' + (err as Error)?.message;
                    console.error(msg);

                    // No error callback implemented. Nothing more we can do here.
                }
            },
            onFailure: (errorCode, errorMessage) => {
                reject(new MqClientApiError(errorMessage, errorCode));
            },
        });

        // Can't fail..?
        resolve({ data: { message: 'registerEventCallback: anonymous callback probably registered' } });
    });
}

// ZZZ: Type more tightly.
export function query(apiMethod: string, parameters: object): Promise<any> {
    // console.log('mqClientApi ' + apiMethod + ' ' + JSON.stringify(parameters));
    // The registerEventCallback is special.
    if (apiMethod === 'registerEventCallback') {
        return Promise.reject(Error('query() called with "registerEventCallback" method. Use the registerEventCallback() function instead.'));
    }
    else
        return new Promise<any>((resolve, reject) => {
            // Perform API request
            cefQuery({
                // Create request string (json string)
                request: JSON.stringify({
                    method: apiMethod,
                    parameters: parameters || null,
                }),

                // Callback on success.
                // Response should always be a JSON string
                onSuccess: (response: string) => {
                    try {
                        // Response should be a JSON string.
                        if (String(response).match(/^Success/)) {
                            // Handle mqCef bug when message isn't encoded as json and string 'Success' is sent
                            resolve({});
                        }
                        else {
                            // Resolve promise
                            resolve(JSON.parse(response));
                        }
                    }
                    catch (err) {
                        const msg = 'Exception in cefQuery.onSuccess in mqClientApi(' + apiMethod + '). Response="' + response + '" Error=' + (err as Error)?.message;
                        console.error(msg);
                        reject(new MqClientApiError(msg, (err as any)?.code, (err as any)?.request));
                    }
                },
                // Callback on error
                onFailure: (errorCode, errorMessage) => {
                    reject(new MqClientApiError(errorMessage, errorCode, 'apiMethod=' + apiMethod));
                },
            });
        });
}
