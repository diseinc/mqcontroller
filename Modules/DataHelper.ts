import type {
    Attribute,
    ChannelInfo,
    GeoPosition,
    Layout,
    PlayerMetadata,
    StringMap
} from "./MqModel.js";
import { PlayerViewMode } from "./MqModel.js";

export const enum ViewMode {
    Edit = 'edit',
    Preview = 'preview',
    LocalChannel = 'local',
    WebChannel = 'server',
    Debug = 'debug',
    Thumbnail = 'thumb',
}

export interface IPlayerData {
    readonly layoutName?: string;
    readonly capabilities: number;
    readonly volume: number;
    readonly geoPosition: GeoPosition;
    readonly layout?: Readonly<Layout>;

    readonly metadata: PlayerMetadata;

    readonly viewMode: ViewMode;
    readonly attributes: StringMap<Attribute | StringMap<Attribute>>;
}

export interface IPlaylistData {
    readonly isFirst: boolean;
    readonly isLast: boolean;
    readonly invocationCount: number;
    readonly priorityLevel: number;
}

export interface ITemplateData {
    // ZZZ: Extends IContentData in Htv.
    settings: Readonly<StringMap<any>>;
}

export interface IClientData {
    id?: string;
    channel: ChannelInfo;
}

export interface IDataHelper {
    type: 'op' | 'htv';
    channel: Readonly<ChannelInfo>;
    client: Readonly<IClientData>;

    // ZZZ. FIX:
    content: Readonly<any>; //Readonly<IContentData>;

    template: Readonly<ITemplateData>;
    player: Readonly<IPlayerData>;
    playlist: Readonly<IPlaylistData>;

    getDuration(defaultSeconds?: number): number;

    // Htv:
    //getExpiry(defaultExpiryMinutes?: number): Date;
    //getFieldValue(fieldName: string, defaultVal?: any, index?: number): any;
    //getMediaFieldFiles(fieldName: string, index?: number): Htv.ContentFile[];
    //getSettingValue(settingName: string, defaultVal?: any): any;
}

export class PlayerData implements IPlayerData {
    public readonly layoutName: string | undefined;
    public readonly capabilities: number;
    public readonly viewCount: number;
    public readonly volume: number;
    public readonly geoPosition: GeoPosition;
    public readonly metadata: PlayerMetadata;
    public readonly layout?: Layout;

    constructor(metadata: PlayerMetadata) {
        this.metadata = metadata;
        this.layoutName = metadata.playerId;
        this.capabilities = metadata.jsPlayerCapabilities || 0;
        this.viewCount = metadata.viewCount;
        this.volume = metadata.volume;
        this.geoPosition = metadata.geoPosition;
        this.layout = metadata.channelLayer;
    }

    #cachedViewMode?: ViewMode;
    public get viewMode(): ViewMode {
        if (this.#cachedViewMode)
            return this.#cachedViewMode;

        if (this.metadata.thumbnailRecordingMode)
            return (this.#cachedViewMode = ViewMode.Thumbnail);

        if (this.metadata.viewMode == PlayerViewMode.Preview)
            return (this.#cachedViewMode = ViewMode.Preview);

        if (this.metadata.viewMode == PlayerViewMode.Edit)
            return (this.#cachedViewMode = ViewMode.Edit);

        if (this.metadata.__isDevHost)
            return (this.#cachedViewMode = ViewMode.Debug);

        // Q: What if we're on Android or other embedded Player?
        if (typeof window.navigator.userAgent === 'string' &&
            window.navigator.userAgent.indexOf(' mqCef/') >= 0)
        return (this.#cachedViewMode = ViewMode.LocalChannel);

        return (this.#cachedViewMode = ViewMode.WebChannel);
    }

    #cachedAttributes?: StringMap<Attribute | StringMap<Attribute>>;
    public get attributes(): Readonly<StringMap<Attribute | StringMap<Attribute>>> {
        if (this.#cachedAttributes)
            return this.#cachedAttributes;

        if (!this.metadata || !this.metadata.attributes)
            return (this.#cachedAttributes = {});

        this.#cachedAttributes = this.metadata.attributes.reduce((o, v) => {
            o[v.name] = v;
            return o;
        }, {} as StringMap<Attribute>);

        const sets: StringMap<StringMap<Attribute>> = {
            player: {},
            static: {},
            dyn: {},
            shared: {},
        };

        for (const attr of this.metadata.attributes) {
            const parts = attr.name.split('.');
            if (parts.length > 1) {
                const coll = sets[parts[0]] || (sets[parts[0]] = {});
                parts.shift();
                coll[parts.join('.')] = attr;
            }
        }
        for (const key in sets)
            this.#cachedAttributes[key] = sets[key];

        return this.#cachedAttributes;
    }
}

export class PlaylistData implements IPlaylistData {
    public readonly isFirst: boolean;
    public readonly isLast: boolean;
    public readonly invocationCount: number = 0;
    public readonly priorityLevel: number = 0;

    constructor(metadata: PlayerMetadata) {
        this.isFirst = !!metadata.firstInPlaylist;
        this.isLast = !!metadata.lastInPlaylist;
        this.invocationCount = metadata.playlistInvocationCount || 0;
        this.priorityLevel = metadata.priorityLevel || 0;
    }
}

class TemplateData implements ITemplateData {
    constructor(metadata: PlayerMetadata) {
        this.settings = metadata.pqiTags ?? {};
    }
    public settings: Readonly<StringMap<any>>;
}

export class DataHelper implements IDataHelper {
    type: 'op' = 'op';

    constructor(public rawData: any, public playerMetadata: PlayerMetadata) {
        this.content = rawData;

        this.channel = rawData?.channel;
        this.client = playerMetadata.htv
            ? {
                id: playerMetadata.htv.clientId,
                channel: playerMetadata.htv.channel,
            }
            : {
                // Old or newer Player.
                id: playerMetadata?.htvClientId || playerMetadata?.playbackDeviceId,
                channel: rawData?.channel,
            };
    }

    public content: any;

    // channel - (ChannelInfo)
    public readonly channel: Readonly<ChannelInfo>;
    // client
    //  id
    //  channel - (ChannelInfo)
    public readonly client: Readonly<IClientData>;

    #cachedTemplate?: TemplateData;
    #cachedPlayer?: PlayerData;
    #cachedPlaylist?: PlaylistData;

    private static cachedHelper: DataHelper;
    private static cachedData: any;

    public get template(): Readonly<ITemplateData> {
        return this.#cachedTemplate || (this.#cachedTemplate = new TemplateData(this.playerMetadata));
    }

    public get player(): Readonly<IPlayerData> {
        return this.#cachedPlayer || (this.#cachedPlayer = new PlayerData(this.playerMetadata));
    }

    // playlist
    //  isFirst
    //  isLast
    //  invocationCount
    //  priorityLevel
    public get playlist(): Readonly<IPlaylistData> {
        return this.#cachedPlaylist || (this.#cachedPlaylist = new PlaylistData(this.playerMetadata));
    }

    public static isAttribute(v: any): v is Attribute {
        return (
            v && typeof v === 'object' && v.name && v.active && typeof v.name === 'string' && typeof v.active === 'boolean'
        );
    }

    public static get(data: any, playerMetadata: PlayerMetadata): DataHelper {
        return DataHelper.cachedData !== data
            ? (DataHelper.cachedHelper = new DataHelper((DataHelper.cachedData = data), playerMetadata))
            : DataHelper.cachedHelper;
    }

    public static getNonCached(data: any, playerMetadata: PlayerMetadata): DataHelper {
        return DataHelper.cachedData !== data ? new DataHelper(data, playerMetadata) : DataHelper.cachedHelper;
    }

    getDuration(defaultSeconds = 10): number {
        const acceptMetadataDurationModes = [ViewMode.Edit, ViewMode.Debug];

        const res = this.player.metadata.duration ||
            (acceptMetadataDurationModes.some((vm) => vm === this.player.viewMode) && this.content?.metadata?._duration) ||
            defaultSeconds;

        return Number(res);
    }
}
