
export function delay(ms: number, rejectOnEnd: true): Promise<never>;
export function delay(ms: number, rejectOnEnd: false): Promise<void>;
export function delay(ms: number, rejectOnEnd: boolean): Promise<void | never>;
export function delay(ms: number, rejectOnEnd: boolean = false): Promise <void | never> {
    return new Promise((resolve, reject) => setTimeout(() => {
        if (rejectOnEnd)
            reject('Timed out');
        else
            resolve();
    }, ms));
}
